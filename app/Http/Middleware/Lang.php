<?php

namespace App\Http\Middleware;

use Closure;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (isset($_GET['lg']) and !empty($_GET['lg'])){
            session()->put('lang',$_GET['lg']);
            app()->setLocale($_GET['lg']);
            return $next($request);
        }elseif (session()->has('lang')){
            app()->setLocale(session()->get('lang'));
            return $next($request);
        }
        else{
            app()->setLocale('ar');
            return $next($request);
        }
    }
}
