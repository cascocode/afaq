<?php

namespace App\Http\Controllers;

use App\Call;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Faker\Generator as Faker;

class CallController extends Controller
{
    public function call(Request $r)
    {

        $data['mail'] = $r->email;
        $data['name'] = $r->name;

        Mail::send('mail',['name'=>$r->name,'mail'=>$r->email,"msg"=>$r->msg], function ($message) use ($data) {
            $message->from("contact@afaqfuture.com", $data['name']);
            $message->subject('Contact Massages');
            $message->to("munira@afaqkw.com");
        });

        $call = new  Call();
        $call->name = $r->name;
        $call->email = $r->email;
        $call->msg = $r->msg;
        $call->save();

        return response()->json($r);

    }

}
