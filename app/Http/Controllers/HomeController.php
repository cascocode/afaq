<?php

namespace App\Http\Controllers;

use App\Archive;
use App\Category;
use App\Project;
use App\ProjectImg;
use App\Invoice;
use App\msgs;
use App\chat;
use App\Settings;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class HomeController extends Controller
{



    public function hom()
    {

        $backimg = Settings::img();
        $projects = \App\Project::all();
        return view('welcome',compact('projects','backimg'));
    }

    public function project($id)
    {
        $n = Project::find($id)->name;
        $d = Project::find($id)->desc;
        $type= Project::find($id)->statues;
        $d_en = Project::find($id)->d_en;
        $n_en = Project::find($id)->n_en;
        if (app()->getLocale() === "ar"){
            $name = $n;
            $desc = $d;
        }else{
            if ($n_en === null){
                $name = $n;
            }else{
                $name = $n_en;
            }
            if ($d_en === null){
                $desc = $d;
            }else{
                $desc = $d_en;
            }
        }
        $imgs = ProjectImg::where('proid', $id)->get();
        return view('project', compact('type','name', 'desc', 'imgs','backimg'));
    }

    public function projectco()
    {
        $backimg = 1;
        $projects = Project::where('statues', 1)->get();
        if (app()->getLocale() === "ar"){
            $name = "المشاريع المنجزة";
        }else{
            $name = "Completed Projects";
        }
        return view('projects', compact('projects','backimg', 'name'));
    }

    public function projectno()
    {
        $projects = Project::where('statues', 0)->get();
        if (app()->getLocale() === "ar") {
            $name = "المشاريع تحت الإنشاء";
        }else{
            $name = "Not Completed Projects";
        }
        $backimg = 0;
        return view('projects', compact('projects','backimg', 'name'));
    }


    public function archives()
    {
        $projects = Category::all();
        $name = "الارشيف";
        $backimg = Settings::img();
        return view('archives', compact('projects', '$backimg', 'name'));
    }

    public function archive($id){
        $backimg = Settings::img();
        if(app()->getLocale() === "ar"){
            $name = Category::find($id)->name;
        } else{
            if(Category::find($id)->en === null){
                $name = Category::find($id)->name;
            }
            $name = Category::find($id)->en;
        }
        $imgs = Archive::where('catid', $id)->get();
        $archive = "";
        return view('project', compact('name', 'imgs','backimg','archive'));
    }

    public function chatmake(Request $r){

        $validatedData = $r->validate([
            "name"=>"required",
            'mail'=>"required"
        ]);
        $check = chat::where("email",$r->mail)->get()->count();
        if ($check == 0):
            $chat = new chat();
            $chat->email = $r->mail;
            $chat->name = $r->name;
            $chat->save();
            cookie()->queue("chat-id",$chat->id,2880);
            $msg = msgs::where("chat_id",$chat->id)->get();
            return json_encode($msg);
        else:
            $chat = chat::where("email",$r->mail)->first();
            cookie()->queue("chat-id",$chat->id,2880);
            $msg = msgs::where("chat_id",$chat->id)->get();
            return json_encode($msg);
        endif;
    }

    public function msgsend(Request $req){
        $chat_id = Cookie::get('chat-id');
        $msg = new msgs();
        $msg->msg = $req->msg;
        $msg->chat_id = $chat_id;
        $msg->save();
        $num = msgs::where('chat_id',$chat_id)->count();
        Mail::send('mail',function ($message) {
            $message->from("contact@afaqfuture.com", "Afaq Future Chat App");
            $message->subject('Contact Massages');
            $message->to("munira@afaqkw.com");
        });
        return 'done';

    }

    public function getmsgs(){
        $id = Cookie::get('chat-id');
        $msgs = msgs::where('chat_id',$id)->get();
        return json_encode($msgs);
    }


    public function invoice($id)
    {
        $inv = Invoice::findOrFail($id);
        if ($inv->status == 1) {
            return view('invoicedone', compact('id', 'inv'));
        } else {
            return view('invoice', compact("inv"));
        }
    }

    function invoicepay($id, Request $req)
    {
        $inv = Invoice::find($id);
        //   Stripe::setApiKey(config("app.strip.secret"));
        Stripe::setApiKey("sk_test_AU1gnzGVa6AYQr9NqmshPqYa");
        $token = $req->stripeToken;
        $charge = Charge::create(
            array(
                'amount' => ($inv->amount * 100),
                'currency' => 'USD',
                'source' => $token
            )
        );
        $inv->status = true;
        $inv->save();
        return redirect(route('home.invoice', $id));
    }
}
