<?php
/**
 * Created by PhpStorm.
 * User: youssef
 * Date: 8/18/18
 * Time: 8:48 PM
 */

namespace App\Http\Controllers;


use App\Settings;

class Functions
{

    public static function getsocial(){
        $set = Settings::all();
        return $set;
    }

    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}