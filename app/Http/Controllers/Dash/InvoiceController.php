<?php

namespace App\Http\Controllers\Dash;
use App\Http\Controllers\Controller;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $invs = Invoice::all();
        return view("dash.invoice.all",compact("invs"));
    }
    public function add(){
        return view("dash.invoice.add");
    }

    public function addH(Request $r){
        Invoice::forceCreate($r->except('_token'));
        return redirect(route("dash.invoice.all"))
            ->with('status', 'success')
            ->with('msg', "تم اضافة الفاتورة ($r->name) بنجاح");

    }

    public function edit($id)
    {
        $inv = Invoice::findOrFail($id);
        return view("dash.invoice.edit",compact("inv"));
    }
    public  function editH(Request $r){
        $inv = Invoice::find($r->id);
        $inv->update($r->except('_token',"id"));

        return redirect(route("dash.invoice.all"))
            ->with('status', 'success')
            ->with('msg', "تم تعديل الفاتورة ($r->name) بنجاح");
    }


    public function delete($id)
    {
        Invoice::destroy($id);
        return redirect(route("dash.invoice.all"))
            ->with('status', 'success')
            ->with('msg', "تم حذف الفاتورة بنجاح");
    }
}

