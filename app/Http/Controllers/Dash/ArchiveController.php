<?php

namespace App\Http\Controllers\Dash;

use App\Archive;
use App\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function add(){
        $cats = Category::all();
        if ($cats->count()) {
            return view("dash.archive.add", compact("cats"));
        }else{
            return redirect(route("dash.cat.add"));
        }
    }
    public  function addH(Request $r){
        $r->validate([
            'catid' => 'required',
        ]);
        if (!filter_var($r->catid,FILTER_VALIDATE_INT)):
            return redirect()->back()->withErrors(['msg'=>"يجب ان تختار القسم الذى تريد ان تكون الملفات تابعة له"]);
        endif;
        $files=array();
        $path = "archive/".$r->catid.'/';
        if($r->hasFile('inputb3')){
            foreach($r->file('inputb3') as $file){
                $name = date('Ymd-s').'-'.$file->getClientOriginalName();
                $file->move($path,$name);
                $item = ["url"=>url($path.$name),"type"=>$file->getClientOriginalExtension(),'path'=>$path.$name];
                $archive = new Archive();
                $archive->catid = $r->catid;
                $archive->files = json_encode($item);
                $archive->save();
            }
            return redirect(route('dash.cat.single',$r->catid));
        }else{
            return redirect()->back()->withErrors(['msg'=>"يجب ان يكون هناك ملف على الاقل تم رفعة"]);
        }
    }
    public  function  delete($id){
        $arc = Archive::findOrFail($id);
        $data = json_decode($arc->files);
        @unlink(public_path($data->path));
        Archive::destroy($id);
        return redirect()->back()->with(['statues'=>"success","msg"=>"تم الحذف بنجاح"]);
    }
}
