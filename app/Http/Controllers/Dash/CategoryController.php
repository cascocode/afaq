<?php

namespace App\Http\Controllers\Dash;

use App\Archive;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Functions;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function index(){
        $cats = Category::all();
        if ($cats->count()) {
            return view("dash.category.all",compact("cats"));
        }else{
            return redirect(route("dash.cat.add"));
        }
    }

    public function single($id){
        $cat = Category::findOrFail($id)->first();
        $name = $cat->name;
        $archives = Archive::all()->where("catid",$id);
        return view('dash.category.single',compact('archives','name'));
    }

    public  function add(){
        return view("dash.category.add");
    }
    public  function addH(Request $r){
        try {
            $cat = new Category();
            $cat->name = $r->name;
            $cat->en = $r->en;
            $cat->save();

            return redirect(route("dash.cat.all"))
                ->with('status', 'success')
                ->with('msg', "تم إذاضفة قسم ($r->name) بنجاح");
        }catch (QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                return redirect(route("dash.cat.all"))
                    ->with('status', 'danger')
                    ->with('msg', "تمت اضافة هذا القسم من قبل");
            }
        }
    }

    public function edit($id)
    {
        $cat = Category::findOrFail($id);
        return view("dash.category.edit",compact("cat"));
    }
    public  function editH(Request $r){
        $cat = Category::find($r->id);
        $cat->name = $r->name;
        $cat->en = $r->en;
        $cat->save();

        return redirect(route("dash.cat.all"))
            ->with('status', 'success')
            ->with('msg', "تم تعديل قسم ($r->name) بنجاح");
    }


    public function delete($id)
    {
        Category::destroy($id);
        Archive::where("catid",$id)->delete();
        Functions::rrmdir(public_path('archive/'.$id));
        return redirect(route("dash.cat.all"))
            ->with('status', 'success')
            ->with('msg', "تم حذف القسم بنجاح");
    }
}
