<?php

namespace App\Http\Controllers\Dash;

use App\Http\Controllers\Controller;
use App\ProjectImg;
use Illuminate\Http\Request;

class ProjectImgController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addview($id){
        return view('dash.projects.addimg',compact('id'));
    }

    public function add($id,Request $r){
        $files=array();
        $path = "projects/".$id.'/';
        if($r->hasFile('inputb3')){
            foreach($r->file('inputb3') as $file){
                $name = date('Ymd-s').'-'.$file->getClientOriginalName();
                $file->move($path,$name);
                $item = ["url"=>url($path.$name),"type"=>$file->getClientOriginalExtension(),'path'=>$path.$name];
                $archive = new ProjectImg();
                $archive->proid = $id;
                $archive->files = json_encode($item);
                $archive->save();
            }
            return redirect(route('dash.project.single',$id));
        }else{
            return redirect()->back()->withErrors(['msg'=>"يجب ان يكون هناك ملف على الاقل تم رفعة"]);
        }
    }


    public  function  delete($id){
        $arc = ProjectImg::findOrFail($id);
        $data = json_decode($arc->files);
        @unlink(public_path($data->path));
        ProjectImg::destroy($id);
        return redirect()->back()->with(['statues'=>"success","msg"=>"تم الحذف بنجاح"]);
    }

}
