<?php

namespace App\Http\Controllers\Dash;

use App\chat;
use App\Http\Controllers\Controller;
use App\Call;
use App\msgs;
use Illuminate\Http\Request;

class CallController extends Controller
{


    public function showall()
    {
        $calls = chat::all();
        return view('dash.calls', compact("calls"));
    }

    public function reply($id)
    {
        $msgs = msgs::where("chat_id",$id)->get();
        return view('dash.calls', compact("msgs"));
    }

    public function del($id){
        Call::destroy($id);
        return redirect()->back();
    }

    public function getch($id,Request $r){
        $i = (int)$id;
        $msgss = msgs::where("chat_id",$i)->get();
        $msgs = json_encode($msgss);
        $call = chat::find($id);
        return view("dash.calls",compact('msgss','call','msgs'));
    }

    public function getchaj($id,Request $r){
        $i = (int)$id;
        $msgs = msgs::where("chat_id",$i)->get();
        $num = $msgs->count();
        $cur = (int) $r->cur;
        if($num > $cur):
            return json_encode($msgs);
        endif;
    }

    public function dele($id){
        chat::destroy($id);
        msgs::where('chat_id',$id)->delete();
        return redirect()->back();
    }

    public function re($id, Request $r){
        $msg = new msgs();
        $msg->chat_id = $id;
        $msg->msg = $r->msg;
        $msg->admin = 1;
        $msg->save();
        return 'done';
     }

}
