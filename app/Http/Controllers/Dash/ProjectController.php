<?php

namespace App\Http\Controllers\Dash;

use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectImg;
use Illuminate\Http\Request;


class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function compAll()
    {
        $projects = Project::where("statues",1)->get();
        $type = "المنجزة";
        return view("dash.projects.all",compact('projects',"type"));
    }

    public function nocompAll(){
        $projects = Project::where("statues",0)->get();
        $type = trans('main.nocom');
        return view("dash.projects.all",compact('projects',"type"));
    }

    public function addview(){
        return view('dash.projects.addp',['type'=>"مكتمل"]);
    }

    public function add(Request $r)
    {
        $validatedData = $r->validate([
            "name"=>"required",
            "statues"=>"required"
        ]);
        try{
            $pro = new Project();
            $pro->statues = $r->statues;
            $pro->name = $r->name;
            $pro->n_en = $r->n_en;
            $pro->desc = $r->desc;
            $pro->loc = $r->loc;
            $pro->d_en = $r->d_en;
            $pro->save();
            return redirect()->back()->with(['statues'=>"success","msg"=>"تمت العملية بنجاح "]);
        }catch (\Illuminate\Database\QueryException $e){
            return redirect()->back()->with(['statues'=>"danger","msg"=>"ربما يكون هذا المشروع موجود من قبل او انه هناك خطأ قد حدث الرجاء المحاولة مرة اخرى او الاتصال بالدعم "]);
        }
    }

    public function editview($id){
        $pro = Project::findOrFail($id);
        $type = "";
        return view('dash.projects.edit',compact('pro','type'));
    }

    public function edit($id,Request $r)
    {
        $validatedData = $r->validate([
            "name" => "required",
            "statues" => "required"
        ]);

        $pro = Project::findOrFail($id);
        $pro->name = $r->name;
        $pro->statues = $r->statues;
        $pro->n_en = $r->n_en;
        $pro->loc = $r->loc;
        $pro->desc = $r->desc;
        $pro->d_en = $r->d_en;
        $pro->save();
        return redirect()->back()->with(['statues'=>"success","msg"=>"تمت العملية بنجاح "]);
    }

    public function single($id){
        $pro = Project::find($id);
        $imgs = ProjectImg::where("proid",$id)->get();
        return view('dash.projects.single',compact('imgs','pro'));
    }

    public  function delete($id){
        Project::destroy($id);
        return redirect()->back();
    }
}
