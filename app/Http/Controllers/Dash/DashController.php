<?php

namespace App\Http\Controllers\Dash;

use App\Http\Controllers\Controller;
use App\Settings;
use App\User;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DashController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home (){
        return view("dash.home");
    }

    public function changePass(Request $request){
        $old = $request->old;
        $new = bcrypt($request->new);
        $id = Auth::user()->id;
        if ( Hash::check($old,User::find($id)->password)):
            $user = User::find($id);
            $user->password = $new;
            $user->save();
            return redirect()->back()->with(['msg'=>"ت تغير كلمة السر بنجاح",'statues'  =>"success"]);
        else:
            return redirect()->back()->withErrors(["لقد ادخلت بيانات غير صحيحة","من فضلط تأطد من البيانات المدخلة"]);
        endif;
    }

    public function changePhoto(Request $request){

        /*
         * $validatedData = $request->validate([
            'img' => 'required|mimes:jpg,png,jpeg'
        ]);
        //File::deleteDirectory(public_path('storage'));
        $url = $request->file('img')->move(public_path('storage/imgs/',$request->file('img')->getClientOriginalName().$request->file('img')->getClientOriginalExtension()));
        dd($url);
        $s = Settings::find(1);
        $s->img = $url;
        $s->save();*/

        return redirect()->back()->with(['msg'=>"This Feature Is Not Working",'statues'  =>"warning"]);

    }

    public function editsoc($id,Request $r){
        $s = Settings::find($id);
        $s->url = $r->url;
        $s->save();
        return redirect()->back();
    }

}
