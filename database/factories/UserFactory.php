<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => "Casco Code",
        'email' => "admin@admin.com",
        'password' => bcrypt('afaq_1234'), // afaq_1234
        'remember_token' => str_random(10)
    ];
});
