<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Settings::class, function (Faker $faker) {
    return [
        'img' => "https://previews.123rf.com/images/korolyok/korolyok1803/korolyok180300130/98425139-vintage-grunge-newspaper-paper-texture-background-blurred-old-newspaper-background-a-blur-unreadable.jpg"
    ];
});
