$(window).on('load', function(){
    setTimeout(function () {
        $("#loading").fadeOut(5000);
        $("#loading").remove();
        $("body").css({
            overflow:"auto"
        });
    },3000);
});

$(document).ready(function () {

    // Loading

    $('#blogCarousel').carousel({
        interval: 5000
    });

    // Smooth scrolling using jQuery easing
    $('a.scroll').not("a[class=slidearrow]").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 100)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

});
$( function() {
    $('#price-range').slider({
        range: true,
        min: 10000,
        max: 15000000,
        values: [10000, 15000000],
        slide: function(event, ui) {
            $('#price-min span').text(ui.values[0]);
           $('#price-max span').text(ui.values[1]);
        }
    });
});
function updateClock() {
    // set the content of the element with the ID time to the formatted string
    $(".timenow").text(new Date(new Date().getTime()).toLocaleTimeString());

    // call this function again in 1000ms
    setTimeout(updateClock, 1000);
}
updateClock(); // initial call


