<?php use App\ProjectImg; ?>
@extends("layouts.app")
@section('page')

    <style>
        #navigation{
            position: relative;
        }
        .cont{
        background:#eeeeee !important;
        }
    </style>

    @if($backimg == 0)
        <section id="top" style="height:90vh">
            <div class="background-image" style="background-size: cover;
                    background-image: url('{{asset('public/home/dist/imgs/pro'.$backimg.".jpg")}}');">
            </div>
            <img class="d-block mx-auto" src="{{asset('public/home/dist/imgs/pro'.$backimg.".jpg")}}" alt="" style="text-align: center;margin: 0 auto !important;height: 100%;">
            <div class="text">
                <h1>{{ $name }} </h1>
                <h3>{{ trans('main.welcome') }} </h3>
            </div>
        </section>
    @else
        <section id="top" style="
        height:80vh;
        background: url({{asset('public/home/dist/imgs/pro'.$backimg.".jpg")}});background-size: cover;" >
            <div class="text">
                <h1>{{ $name }} </h1>
                <h3>{{ trans('main.welcome') }} </h3>
            </div>
        </section>
    @endif


    <!-- Page Content -->

    <section class="py-5" id="aboutus">
        <div class="container">
            <div class="row rt">
                <div class="title text-center">
                    <h1> مشاريعنا</h1>
                </div>
            </div>
            <div class="row cont justify-content-center">
                @foreach($projects as $pro)
                    <div class="col-md-3 text-center slidimg">
                        <a href="{{ route('project',$pro->id) }}">
                        <?php $file = @json_decode(ProjectImg::where("proid",$pro->id)->first()->files); ?>
                        <img class="img-thumbnail" src="{{ $file->url or 132 }}" alt="">
                            <h3>
                                @if(app()->getLocale() === "ar") {{ $pro->name }} @else  @if($pro->n_en === null) {{ $pro->name }} @endif {{ $pro->n_en }} @endif
                            </h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


@endsection
@push('css')
    <style>
           .background-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            filter: blur(12px);
            position: absolute;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 0;
        }
        #top{
            position: relative;
        }
        #top img{
            position: absolute;
        }
        #top .text{
            z-index: 2;

            margin-top: 200px;
        }
 
        .slidimg{
            width: 250px;
        }
        .slidimg img{
            width: 100%;
            height: 250px;
        }

    </style>
@endpush
