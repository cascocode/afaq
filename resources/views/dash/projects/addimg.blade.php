@extends("layouts.dash")
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <h1 class="title">اضافة عنصر الى المشروع</h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('dash.project.add.imgp',$id) }}" method="post" class=""  enctype="multipart/form-data">
                @csrf
                <div dir="ltr" style="text-align: right">
                <input id="input-b3" name="inputb3[]" type="file" class="file col-md-12" multiple
                       data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload...">
                 </div>
                <input type="submit" class="btn btn-primary col-md-2 float-left"  >
            </form>
        </div>
    </div>
@endsection
@push("css")

@endpush
@push("js")
    <script>
        // initialize with defaults
        $("#input-b3").fileinput({
            language: "ar",
            dropzone:true
        });
    </script>
@endpush
