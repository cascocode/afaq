@extends("layouts.dash")
@section('content')
    <div class="row">
        <div class="col-md-4">
            <h2 class="title">{{ $pro->name }}</h2>
        </div>
        <div class="col-md-4 mt-1">
            <a href="{{ route('dash.project.add.img',$pro->id) }}" class="btn btn-primary">اضف عنصر</a>
        </div>
        <div class="col-md-12 m-2">
            <textarea class="form-control bg-white font-weight-bold" readonly>{{ $pro->desc }}</textarea>
        </div>
    </div>
    @if(\Session::has("statues"))
        <div class="alert alert-dismissible alert-{{ \Session::get('statues') }}">
            <button class="close" type="button" data-dismiss="alert">×</button>
            {{ \Session::get('msg') }}</a>
        </div>
    @endif
    <div class="row m-2" id="gallery">
        @forelse($imgs as $key => $img)
            <?php $file = json_decode($img->files); ?>
            @if($file->type == 'mp4')
                <div class="item" data-order="{{ $key }}">
                    <video class="file-preview-video" src="{{ $file->url }}" controls></video>
                </div>
            @endif
            <div class="item col-md-3 mb-2">
                <a href="{{ route('dash.project.delete.img',$img->id) }}"><i class="fa fa-trash"></i></a>
                <img class="rounded mx-auto d-block img-thumbnail" src="{{ $file->url }}">
            </div>
        @empty
        @endforelse
    </div>
@endsection


@push("css")
    <style>
        .item a {
            position: absolute;
            top: 5px;
            right: 15px;
            padding: 2px 5px;
            background: #f00;
            color: #fff;
            font-size: 18px;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
        }

        .item a:hover {
            color: #f00;
            background: white;
        }

        .item img {
            width: 100%;
        }
    </style>
@endpush
