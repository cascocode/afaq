    @extends("layouts.dash")
    @section('content')
        <div class="row">

            @if(session()->has('msg'))
                <div class="alert alert-dismissible alert-{{ session()->get('statues') }}">
                    <button class="close" type="button" data-dismiss="alert">×</button>{{ session()->get('msg') }}</a>.
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-12">
                <h1 class="title">تعديل مشروع {{ $type }}</h1>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('dash.project.editp',$pro->id) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="from-group col-md-6">
                            <label for="#name" title="name">الاسم</label>
                            <input value="{{ $pro->name }}" type="text" id="name" title="name" name="name" class="form-control col-md-10">
                        </div>

                        <div class="from-group col-md-6">
                            <label for="#desc" title="name">النبذة</label>
                            <textarea type="text" id="desc" title="desc" name="desc" class="form-control col-md-10">{{ $pro->desc }}
                            </textarea>
                        </div>
                        <br>
                        <hr style="width: 100%;height: 2px;background: #eee">
                        <br>
                        <h4 class="col-md-12">الترجمة</h4>
                        <div class="from-group col-md-6">
                            <label for="#name" title="name">الاسم</label>
                            <input value="{{ $pro->n_en }}" type="text" id="n_en" title="name" name="n_en" class="form-control col-md-10">
                        </div>

                        <div class="from-group col-md-6">
                            <label for="#desc" title="name">النبذة</label>
                            <textarea type="text" id="d_en" title="desc" name="d_en" class="form-control col-md-10">{{ $pro->d_en }}
                            </textarea>
                        </div>

                        <hr style="width: 100%;height: 2px;background: #eee">
                        <br>
                        <div class="form-group col-md-12">
                            <label for="#loc" title="Location">{{ trans("main.loc") }}</label>
                            <input class="form-control" id="loc" type="url" name="loc" value="{{ $pro->loc or "" }}">
                        </div>
                        <div class="from-group col-md-6">
                            <label for="#type" title="type">النوع</label>
                            <select type="text" id="type" title="type" name="statues" class="form-control col-md-10">

                                <option value="1">مكتمل</option>

                                <option {{ $pro->statues = ($pro->statues  == 0 ? "selected":' ') }} value="0">غير مكتمل</option>

                            </select>
                        </div>
                    </div>

                    <hr style="width: 100%;height: 2px;background: #eee">
                    <br>
                        <button class="btn btn-primary m-2" type="submit">اضف الان</button>
                </form>
            </div>
        </div>
    @endsection
    @push("css")
        <link rel="stylesheet" href="{{ asset('dash/dropzone/basic.css') }}">
        <link rel="stylesheet" href="{{ asset('dash/dropzone/dropzone.css') }}">
    @endpush
    @push("js")
        <script src="{{ asset('dash/dropzone/dropzone.js') }}"></script>
        <script>
            $("#myform").dropzone({
                url: "{{ route('dash.project.add') }}",
                acceptedFiles: "image/*",
            });
        </script>
    @endpush
