@extends("layouts.dash")danger
@section('content')
    @if(\Session::has("status"))
        <div class="alert alert-dismissible alert-{{ \Session::get('status') }}">
            <button class="close" type="button" data-dismiss="alert">×</button>{{ \Session::get('msg') }}</a>.
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <h1 class="title">المشاريع  {{ $type }}</h1>
        </div>
        <div class="col-md-4 mt-1">
            <a href="{{ route('dash.project.add') }}" class="btn btn-primary">اضف مشروع</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><h4>#</h4></th>
                        <th><h4>المشروع</h4></th>
                        <th><h4>اجراء</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $key => $cat)
                        <tr>
                            <td><h6>{{ $key + 1 }}</h6></td>
                            <td><h6><a href="{{ route("dash.project.single",$cat->id) }}">{{ $cat->name }}</a></h6></td>
                            <td>
                                <h6 class="btn-group " dir="ltr">
                                    <a href="{{ route("dash.project.delete", $cat->id) }}" class="btn btn-danger">حذف</a>
                                    <a href="{{ route("dash.project.single",$cat->id) }}" class="btn btn-success">عرض</a>
                                    <a href="{{ route("dash.project.edit", $cat->id)  }}" class="btn btn-warning">تعديل</a>
                                </h6>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
