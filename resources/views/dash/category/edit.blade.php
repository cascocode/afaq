@extends("layouts.dash")
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">اضافة قسم</h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('dash.catedit') }}" method="post" class="row">
                @csrf
                <input type="hidden" name="id" value="{{ $cat->id }}" />

                <div class="form-group col-md-12">
                    <label>الاسم بالعربية</label>
                    <input value="{{ $cat->name }}" type="text" name="name"  class="form-control col-md-10" />
                    <br><br>
                </div>
                <div class="form-group col-md-12">
                    <label>الاسم بالانجليوية</label>
                    <input value="{{ $cat->en }}" type="text" name="en"  class="form-control col-md-10" />
                    <br><br>
                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary col-md-2">اضافة
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
