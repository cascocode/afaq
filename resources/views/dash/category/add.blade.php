@extends("layouts.dash")
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">اضافة قسم</h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('dash.catadd') }}" method="post" class="row">
                @csrf
                <div class="form-group col-md-12">
                    <label>الاسم بالعربية</label>
                <input type="text" name="name"  class="form-control col-md-10" />
                <br><br>
                </div>
                <div class="form-group col-md-12">
                    <label>الاسم بالانجليوية</label>
                    <input type="text" name="en"  class="form-control col-md-10" />
                    <br><br>
                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-primary col-md-2">اضافة
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push("css")
    <link rel="stylesheet" href="{{ asset('dash/dropzone/basic.css') }}">
    <link rel="stylesheet" href="{{ asset('dash/dropzone/dropzone.css') }}">
@endpush
@push("js")
    <script src="{{ asset('dash/dropzone/dropzone.js') }}"></script>
    <script>
        $("#myform").dropzone({
            url: "{{ route('dash.project.add') }}",
            acceptedFiles:"image/*",
        });
    </script>
@endpush
