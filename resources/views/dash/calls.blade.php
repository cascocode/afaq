@extends("layouts.dash")

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">الرسائل</h1>
        </div>
    </div>
    <br>
    @if(isset($calls))
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th><h4>#</h4></th>
                        <th><h4>من</h4></th>
                        <th><h4>رد</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($calls as $key => $call)
                        <tr>
                            <td><h6>{{ $key + 1 }}</h6></td>
                            <td><h6>{{ $call->name ." | ". $call->email }}</h6></td>
                            <td>
                                <a type="button" href="{{ route('dash.msg.get',$call->id) }}" class="btn btn-primary">
                                    عرض المحادثة
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

    @if(isset($msgs))
    <div id="chat">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $call->name }}<br> <p>{{ $call->email }}</p> </h5>
                </div>
                <div class="modal-body">
                    <div class="tile" style="direction: initial">
                        <div class="messanger">
                            <div class="messages">
                                @foreach($msgss as $msg )
                                    @if($msg->admin == 1)
                                        <div class="message me">
                                            <img style="height:40px" src="{{ asset('public/home/dist/imgs/ic_logo.png') }}" alt="Picture">
                                            <p class="info"> {{ $msg->msg }} <br> <span style="font-size: 10px;margin-top: 15px">{{ $msg->created_at }}</span> </p>
                                        </div>
                                    @else
                                        <div class="message">
                                            <p class="info"> {{ $msg->msg }} <br> <span style="font-size: 10px;margin-top: 15px">{{ $msg->created_at }}</span> </p>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="bottom">

                                </div>
                            </div>
                            <form class="sender" id="send{{ $call->id }}" action="{{ route('dash.re.call',$call->id) }}">
                                <input type="text" placeholder="Send Message" name="msg">
                                <button data-target="send{{ $call->id }}" class="btn btn-primary" type="submit"><i class="fa fa-lg fa-fw fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push("js")
            <script>

               function updatechat(cur){
                   let img = "{{ asset('public/home/dist/imgs/ic_logo.png') }}";
                   $.ajax({
                       url: "{{ route("dash.msg.getj",$call->id) }}",
                       type:"POST",
                       dataType:"JSON",
                       data:{_token:"{{ csrf_token() }}",cur:cur},
                       success: function (data) {
                           $("#chat .messages").empty();
                           $.each(data, function (i,msg) {
                               if ( msg.admin !== 1 ){
                                   $("#chat .messages").append(
                                       '<div class="message">\n' +
                                       '            <p class="info">' + msg.msg + ' <br> <span style="font-size: 10px;margin-top: 15px">' + msg.created_at +'</span></p>\n' +
                                       '</div>');
                               } else {
                                   $("#chat .messages").append(
                                       '<div class="message me">\n' +
                                       '            <img style="height:40px" src="'+ img +'" alt="Picture">\n' +
                                       '            <p class="info">' + msg.msg + ' <br> <span style="font-size: 10px;margin-top: 15px">' + msg.created_at +'</span></p>\n' +
                                       '</div>\n');
                               }
                           });
                       }
                   });
               }

               var sett = null;
               $("#chat").ready(function () {
                    let target = $(this);
                    let img = "{{ asset('public/home/dist/imgs/ic_logo.png') }}";
                    let cur = $("#chat .modal-dialog .modal-body .messanger .messages .message").length;
                    sett = setInterval(function () {
                        updatechat(cur,target)
                    },1000);
                });
                $(".sender").submit(function (e) {
                    e.preventDefault();
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    let token = "{{ csrf_token() }}";
                    let msg = $(this).find('input').val();
                    var input = $(this).find('input');
                    $.ajax({
                        url: $(this).attr('action'),
                        type:"POST",
                        data:{_token:token,msg:msg},
                        success:function (data) {
                            input.val('');
                        }
                    });
                    return false;
                });
            </script>
        @endpush
    @endif

@endsection
