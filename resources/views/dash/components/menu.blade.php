<ul class="app-menu">
    <li><a class="app-menu__item" href="{{ route('dash.home') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">الرئيسية</span></a></li>
    <li><a class="app-menu__item" href="{{ route('dash.call') }}"><i class="app-menu__icon fa fa-envelope"></i><span
                class="app-menu__label">رسائل</span></a></li>
    <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-money"></i><span class="app-menu__label">الفواتير</span><i class="treeview-indicator fa fa-angle-right"></i></a>
        <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{  route('dash.invoice.all') }}"><i class="icon fa fa-circle-o"></i> كل الفواتير</a></li>
            <li><a class="treeview-item" href="{{  route('dash.invoice.add') }}"><i class="icon fa fa-circle-o"></i> اضافة فاتورة</a></li>
        </ul>
    </li>
    <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-building"></i><span class="app-menu__label">المشاريع</span><i class="treeview-indicator fa fa-angle-right"></i></a>
        <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{ route('dash.projects.complete.all') }}"><i class="icon fa fa-circle-o"></i> مشاريع مكتملة</a></li>
            <li><a class="treeview-item" href="{{ route('dash.projects.nocomplete.all') }}"><i class="icon fa fa-circle-o"></i> مشاريع تحت الانشاء</a></li>
        </ul>
    </li>

    <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-folder-open"></i><span class="app-menu__label">الارشيف</span><i class="treeview-indicator fa fa-angle-right"></i></a>
        <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{ route('dash.cat.all') }}"><i class="icon fa fa-circle-o"></i>الأقسام</a></li>
            <li><a class="treeview-item" href="{{ route('dash.cat.add') }}"><i class="icon fa fa-circle-o"></i> اضافة قسم</a></li>
            <li><a class="treeview-item" href="{{ route('dash.archive.add') }}"><i class="icon fa fa-archive"></i>اضافة ارشيف تابع لقسم</a></li>
        </ul>
    </li>

</ul>
