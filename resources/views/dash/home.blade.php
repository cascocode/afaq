@inject('function', 'App\Http\Controllers\Functions')
@extends("layouts.dash")

@section("content")
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    @if(session()->has('statues'))
        <div class="alert alert-success">
            <h4>{{ session()->get('msg') }}</h4>
        </div>
        @endif
        <div class="tile">
            <h3>الرئيسية</h3>
        </div>
        <div class="row">
            <button onclick="$('#settings').slideToggle(500)" class="btn text-white" style="background: #0a6ebd"> <i class="fa fa-cog"></i> الاعدادات </button>
        </div>
        <div class="row m-4" id="settings" style="display: none;">
            <h2> الاعدادات</h2>
            <hr style="width: 100%; height: 2px; background: grey"> <br>
            <div class="col-md-12 p-2">
                <h4> تغير كلمة السر</h4>
                <form action="{{ route('dash.change.pass') }}" class="col-md-12" method="post">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <input autocomplete="false" class="form-control col-md-4" type="password" name="old" placeholder="The Old Password">
                            <input autocomplete="false" class="form-control col-md-4 mr-2" type="password" name="new" placeholder="The New Password">
                            <button class="btn bg-secondary text-white" type="submit">حفظ </button>
                        </div>
                    </div>
                </form>
            </div>
            <hr style="width: 100%; height: 2px; background: grey"> <br>
            <div class="col-md-12 p-2">
                <h4> تعديل السوشيال</h4>
                @foreach($function->getsocial() as $s)
                    <form action="{{ route('dash.change.soc',$s->id) }}" class="col-md-12 p-3" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="row">
                                @if($s->name == "insta")
                                <h2 class="m-2"><i class="fa fa-instagram"></i></h2>
                                @endif
                                @if($s->name == "face")
                                <h2 class="m-2"><i class="fa fa-facebook"></i></h2>
                                @endif
                                @if($s->name == "twit")
                                <h2 class="m-2"><i class="fa fa-twitter"></i></h2>
                                @endif
                                @if($s->name == "snap")
                                <h2 class="m-2"><i class="fa fa-snapchat"></i></h2>
                                @endif
                                @if($s->name == "yout")
                                <h2 class="m-2"><i class="fa fa-youtube"></i></h2>
                                @endif
                                <input value="{{ $s->url }}" autocomplete="false" class="form-control col-md-4" type="url" name="url" placeholder="The Old Password">
                                <button class="btn bg-secondary text-white" type="submit">حفظ </button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>

        </div>
    </div>
@endsection