@extends("layouts.dash")
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">اضافة فاتورة</h1>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('dash.invoiceedit') }}" method="post" class="row">
                @csrf
                <input value="{{ $inv->id}}" hidden type="hidden" name="id" >
                <input type="text" value="{{ $inv->name }}" name="name" placeholder="الاسم*" class="form-control col-md-6" required />
                <input type="email" value="{{ $inv->email }}" name="email" placeholder="بريده الإلكتروني" class="form-control col-md-6" />
                <input type="text" value="{{ $inv->apartment }}" name="apartment" placeholder="العقار*" class="form-control col-md-6" />
                <input type="number" value="{{ $inv->amount }}" placeholder="0" required  min="0" value="" step="0.01"  pattern="^\d+(?:\.\d{1,2})?$" name="amount" placeholder="المبلغ*" class="form-control col-md-6" />
                <textarea name="notes"  placeholder="المزيد من التفاصيل" rows="4" class="col-md-12 form-control">{{ $inv->notes }}</textarea>
                <input type="submit" class="btn btn-primary col-md-2"  >
            </form>
        </div>
    </div>
@endsection
@push("css")
@endpush
@push("js")
@endpush
