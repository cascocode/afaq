@extends("layouts.dash")
@section('content')
    @if(\Session::has("status"))
        <div class="alert alert-dismissible alert-{{ \Session::get('status') }}">
            <button class="close" type="button" data-dismiss="alert">×</button>{{ \Session::get('msg') }}</a>.
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <h1 class="title">جميع الفواتير</h1>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <table class="table table-bordered table-responsive">
                    <thead>
                    <h4>الفاتورة</h4>
                    <hr>
                    <tr>
                        <th><h5>#</h5></th>
                        <th><h5>الاسم</h5></th>
                        <th><h5>بريده الإلكتروني</h5></th>
                        <th><h5>العقار</h5></th>
                        <th><h5>المبلغ الطلوب</h5></th>
                        <th><h5>مزيد من المعلومات</h5></th>
                        <th><h5>الحالة</h5></th>
                        <th><h5>اجراء</h5></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invs as $key => $inv)
                        <tr>
                            <td><p>{{ $key + 1 }}</p></td>
                            <td><p>{{ $inv->name }}</p></td>
                            <td><p><a href="mailto:{{ $inv->email}}">{{ $inv->email }}</a></p></td>
                            <td><p>{{ $inv->apartment }}</p></td>
                            <td><p>{{ $inv->amount }}</p></td>
                            <td><p><textarea class="form-control">{{ $inv->notes }}</textarea></p></td>
                            <td>{!! ($inv->status == 1 ? '<h4 class="badge badge-info">مدفوعه</h4>':'<h4 class="badge badge-danger">غير مدفوعه</a>') !!} </td>
                            <td>
                                <p class="btn-group " dir="ltr">
                                    <a href="{{ route("dash.invoice.delete", $inv->id) }}" class="btn btn-danger">حذف</a>
                                    <a href="{{ route("home.invoice", $inv->id) }}" target="_blank" class="btn btn-success">عرض</a>
                                    <a href="{{ route("dash.invoice.edit", $inv->id)  }}" class="btn btn-warning">تعديل</a>
                                    <button data-clipboard-text="{{ route("home.invoice", $inv->id) }}"  class="btn btn-info">رابط الفاتورة</button>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="https://rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js" ></script>
    <script>
        $(document).ready(function(){

            $('.btn').tooltip({
                trigger: 'click',
                placement: 'bottom'
            });

            function setTooltip(btn, message) {
                btn.tooltip('hide')
                    .attr('data-original-title', message)
                    .tooltip('show');
            }

            function hideTooltip(btn) {
                setTimeout(function() {
                    btn.tooltip('hide');
                }, 1000);
            }

            // Clipboard

            var clipboard = new ClipboardJS('.btn');

            clipboard.on('success', function(e) {
                var btn = $(e.trigger);
                setTooltip(btn, 'تم النسخ');
                hideTooltip(btn);
            });
        });
    </script>
@endpush
