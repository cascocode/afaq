<?php use App\ProjectImg; ?>
@extends("layouts.app")
@section('page')
    <style>
        #navigation{
            position: relative;
        }
    </style>
    <section id="top" style="background: url('{{asset('public/a3.jpg')}}'); background-size: cover;">
        <div class="text">
            <h1> {{ trans('main.archive') }}</h1>
        </div>
    </section>


    <!-- Page Content -->

    <section class="py-5" id="aboutus">
        <div class="container">
            <div class="row rt">
                <div class="title text-center">
                    
                </div>
            </div>
            <div class="row cont">
                @foreach($projects as $pro)
                    <div class="col-md-3 text-center">
                        <h3>
                            <a href="{{ route('archive',$pro->id) }}">
                            @if(app()->getLocale() === "ar") <i class="icofont-folder"></i> {{ $pro->name }} @else <i class="icofont-folder"></i> @if($pro->en === null) {{ $pro->name }} @endif {{ $pro->en }} @endif
                            </a>
                        </h3></div>
                @endforeach
            </div>
        </div>
    </section>


@endsection