<?php error_reporting(0 ) ?>
@extends("layouts.app")
@section('page')
    <?php @$fi = json_decode($imgs[1]->files); ?>
    <section id="top">
        <div class="background-image" style="background-size: cover;
                background-image: url('{{ $fi->url }}');">
        </div>
        <img class="d-block mx-auto" src="{{ $fi->url }}" alt="" style="text-align: center;margin: 0 auto !important;height: 100%;">
        <div class="text">
            <h1>{{ $name }} </h1>
            <h3>{{ trans('main.welcome') }}</h3>
        </div>
    </section>


    <section id="page" class="py-4">
        <div class="container" id="projects">
            <h1 class="text-center p-2 title">
                {{ $name }}
            </h1>
            <div class="col-md-12">
                <p readonly="" class="text-center p-2 title bg-white">{{ $desc }}</p>
            </div>
            <div class="col-md-8 offset-2 p-2 text-right">
                <a style="font-size: 20px" href="{{ $loc }}"><i class="icofont-location-pin"></i>{{ trans('main.loc') }}</a>
            </div>
            <div class="row cont p-3">

                @if($type == 0  )
                    @for($i =0; $i<=$imgs->count()  ; $i++)
                        <?php @$file = json_decode($imgs[$i]->files); ?>
                        <div class="col-md-3 m-1">
                            @if(in_array($file->type,['mp4','3gp','MP4',"Mp4","mP4"]))
                                <a href="{{{ @$file->url }}}" data-toggle="lightbox" data-gallery="example-gallery">
                                    <video class="embed-responsive" src="{{ $file->url }}"></video>
                                </a>
                            @else
                                <a href="{{{ @$file->url }}}" data-toggle="lightbox" data-gallery="example-gallery">
                                    <img style="width: 100%;height: 200px" src="{{ $file->url }}" alt="">
                                </a>
                            @endif
                        </div>
                        @if($i == 5)
                            <hr style="width: 100%;height: 9px;background: #eee;">
                            <h1 class="col-md-12 mr-5 p-2">{{ trans('main.imgs') }}</h1>
                        @endif
                    @endfor
                @else
                @foreach($imgs as $img)
                    <?php @$file = json_decode($img->files); ?>
                    <div class="col-md-3 m-1">
                        @if(in_array($file->type,['mp4','3gp','MP4',"Mp4","mP4"]))
                            <a href="{{{ @$file->url }}}" data-toggle="lightbox" data-gallery="example-gallery">
                                <video class="embed-responsive" src="{{ $file->url }}"></video>
                            </a>
                        @else
                            <a href="{{{ @$file->url }}}" data-toggle="lightbox" data-gallery="example-gallery">
                            <img style="width: 100%;height: 200px" src="{{ $file->url }}" alt="">
                            </a>
                        @endif
                    </div>
                @endforeach
                    @endif
            </div>
        </div>
    </section>


@endsection
@push('css')
    <style>
        .background-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            filter: blur(12px);
            position: absolute;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 0;
        }
        #top{
            position: relative;
        }
        #top img{
            position: absolute;
        }
        #top .text{
            z-index: 2;

            margin-top: 200px;
        }
    </style>
@endpush