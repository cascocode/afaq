<?php
use App\ProjectImg; ?>
@extends("layouts.app")
@section('page')


    <header id="slide">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                @foreach($projects as $key => $pro)
                    @if(\App\ProjectImg::where("proid",$pro->id)->count() > 0)
                        <?php $file = json_decode(ProjectImg::where("proid", $pro->id)->first()->files); ?>
                        <div class="carousel-item @if($key == 0) active @endif">
                            @else
                                <div class="carousel-item @if($key == 0) active @endif justify-content-center">
                                    @endif
                                    <div class="background-image" style="background-image: url('{{ $file->url }}');"></div>
                                    <img class="d-block mx-auto" src="{{ $file->url }}" alt="" style="text-align: center;margin: 0 auto !important;height: 100%;">
                                    <div class="carousel-caption  d-md-block" style="bottom: inherit;
                                    position: absolute;top: 500px">
                                        <a href="{{ route('project',$pro->id) }}" class="text-white"><h3>
                                                @if(app()->getLocale() === "ar") {{ $pro->name }} @else  @if($pro->n_en === null) {{ $pro->name }} @endif {{ $pro->n_en }} @endif
                                            </h3></a>
                                    </div>
                                </div>
                                @endforeach
                        </div>
                        <a class="slidearrow carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <i class="icofont-curved-double-left icofont-3x"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="slidearrow carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <i class="icofont-curved-double-right icofont-3x"></i>
                            <span class="sr-only">Next</span>
                        </a>
            </div>
        </div>
    </header>
    <!-- Completed Projects -->
    <section class="py-5" id="aboutus">
            <div class="container">
                <div class="row rt">
                    <div class="title text-center">
                        <h1>{{ trans('main.projects') }}</h1>
                    </div>
                </div>
                <div class="row cont">
                    @foreach($projects as $pro)
                        <div class="col-md-3 text-center">
                                    <div class="slidimg">
                                        <?php
                                        @$vi = ProjectImg::where('proid',$pro->id)->limit(3)->get()[2]->files;
                                        @$v = json_decode($vi)->url;
                                        @$pi = ProjectImg::where('proid',$pro->id)->get()[0]->files;
                                        @$p = json_decode($pi)->url;

                                        ?>
                                            <a href="{{{ @$v }}}" data-toggle="lightbox" data-gallery="example-gallery">
                                                <img style="width: 100%;height: 200px" src="{{ $p }}" alt="">
                                            </a>
                                </div>
                            <a href="{{ route('project',$pro->id) }}">

                            <h3>
                                @if(app()->getLocale() === "ar") {{ $pro->name }} @else  @if($pro->n_en === null) {{ $pro->name }} @endif {{ $pro->n_en }} @endif
                            </h3>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>


@endsection

@push('css')
    <style>
        .slidimg{
            width: 250px;
        }
        .slidimg img{
            width: 100%;
            height: 250px;
        }
        .background-image {
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
            width: 100vw;
            /* overflow: hidden; */
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: -1;
            filter: blur(12px);
        }
        #slide{
            margin-top: 0 !important;
        }
    </style>
@endpush
@push('js')
    <script>
        $("#myCarousel").carousel   ();
    </script>
@endpush