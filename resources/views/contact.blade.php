
@extends('layouts.app')

@section('page')
    <style>
        #navigation{
            position: relative;
        }
    </style>
    <section id="top" style="background: url('{{asset('public/home/dist/imgs/content.jpg')}}'); background-size: cover;">
        <div class="text">
            <h1>{{ trans('main.callus') }} </h1>
        </div>
    </section>

    @if(app()->getLocale() === "ar")
    <!-- Page Content -->
    <section id="page" class="py-4">
        <div class="container">

            <!-- Our Vision Section Nad More -->

            <section id="about" class="py-3">
                <div class="container p-2">
                    <div class="row cont">
                        <div class="row text-right">
                            <div class="col-md-12">
                                <h1 style="width: 100%">اتصل بنا</h1>
                                <br>
 <h1  style="color:#2B37B4"> <strong> مؤسسة آفاق المستقبل العقارية</strong></h1>
                               
                                <br>
                                <h4>رقم التليفون :
                                    <br>
                                    <a href="tel:0096522250028">0096522250028</a>    &nbsp;   -    &nbsp; <a
                                            href="tel:0096522250027">0096522250027</a>
                                    <br>
                                    رقم المحمول :
                                    <br>
                                    <a href="tel:0096551130250">0096551130250   </a>    &nbsp;   -    &nbsp; <a
                                            href="tel:0096552229974">0096552229974</a>

                                </h4>
                                <br>
                                <br>

                                <h4>  فاكس: 0096522250029</h4>
                                <br>
                                 <h4>
                                    البريد الالكترونى :info@afaqkw.com
                                </h4>
                                <br>
                                                                <h4>العنوان : الكويت شرق  -  أبراج مزايا   -  برج مزايا 2   -   الطابق 22                               </h4>


                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </section>

            <hr style="margin: 0;height:10px;background: #eee">

        </div>
    </section>
    @else
    <section id="page" class="py-4">
        <div class="container">

            <!-- Our Vision Section Nad More -->

            <section id="about" class="py-3">
                <div class="container p-2">
                    <div class="row cont">
                        <div class="row text-left p-4">
                            <div class="col-md-12">
                                <h1 style="width: 100%">Call Us</h1>
                                <br>
 <h1  style="color:#2B37B4"> <strong> مؤسسة آفاق المستقبل العقارية</strong></h1>

                                <br>
                                <h4>رقم التليفون :
                                    <br>
                                    <a href="tel:0096522250028">0096522250028</a>    &nbsp;   -    &nbsp; <a
                                            href="tel:0096522250027">0096522250027</a>
                                    <br>
                                    رقم المحمول :
                                    <br>
                                    <a href="tel:0096551130250">0096551130250   </a>    &nbsp;   -    &nbsp; <a
                                            href="tel:0096552229974">0096552229974</a>

                                </h4>
                                <br>
                                <br>

                                <h4>  فاكس: 0096522250029</h4>
                                <br>
                                 <h4>
                                    البريد الالكترونى :info@afaqkw.com
                                </h4>
                                <br>
                                                                <h4>العنوان : الكويت شرق  -  أبراج مزايا   -  برج مزايا 2   -   الطابق 22                               </h4>


                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </section>

            <hr style="margin: 0;height:10px;background: #eee">

        </div>
    </section>
    @endif

@endsection
