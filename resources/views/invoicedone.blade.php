<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>دفع الفواتير</title>
    <!-- The Styling File -->
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo|Open+Sans" rel="stylesheet">
    <style>
        /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
        * {
            font-family: 'Cairo', 'Open Sans', sans-serif;
        }

        body {
            background: url("{{ asset('public/home/dist/imgs/about.jpg') }}");
            background-size: cover;
        }

        body::before {
            content: "";
            width: 100%;
            height: 100%;
            background: #333;
            opacity: 0.2;
            position: absolute;
        }

        .container {
            position: relative;
            z-index: 50;
        }

        .StripeElement {
            background-color: white;
            height: 40px;
            padding: 10px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-right bg-dark">
                    <img src="{{ asset("public/css/ic_logo.png") }}" height="70px">
                    <h1 class="float-left text-white">فاتورة #{{ $inv->id }}</h1>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <h3 class="alert text-center alert-success col-md-10">تم الدفع بنجاح 🙂</h3>
                        <div class="col-md-6 text-center">
                            <img style="width: 100px" class="" src="https://image.flaticon.com/icons/svg/214/214343.svg" alt="">
                            <h5>عملية شراء امنة </h5>
                            <h5 class="p-2" style="background: #eee; color: #13bd0c">{{ $inv->amount }} التكلفة:</h5>
                        </div>
                        <div class="col-md-6">
                            <h5 class="text-right">معلومات عن الفاتورة</h5><br>
                            <p class="text-right">
                                {{ $inv->name .' :الاسم' }}<br>
                                {{ $inv->apartment .' :الغقار' }}<br>
                                {{ $inv->email .' :الالبريد الالكترونى' }}<br>
                                {{ $inv->notes .'  : ملاحظات' }}<br>
                            </p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
<!-- Stripe JS -->
<script src="{{ asset('public/home/dist/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/bootstrap.js') }}"></script>

</body>
</html>
