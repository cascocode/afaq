<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="margin: 0px auto;">
<div style="text-align: right; direction: rtl;">
    <div style="background: #eee; text-align: right">
        <img style="width: 120px; height: 120px;position: relative;top: 15px; border-radius: 100%;" src="{{ asset('public/home/dist/imgs/ic_logo.png') }}">
        <h1 style="display: inline-block;margin-right: 15px;position: relative;">افاق المستقبل العقارية</h1>
    </div>
    <h1 style="color: purple">رسالة جديدة من العملاء</h1>
    <h2 style="margin: 0">هناك رسالة جديدة من العملاء بأنتظار الرد عليها </h2>
    <h3>
        برجاء الضغط على الرابط للوصول الى لوحة التحكم
    </h3>
    <div style="text-align: center;padding: 10px; margin-bottom: 20px">
        <a style="text-decoration: none;padding: 10px;background: #ff0000;color: #fff;font-size: 20px" href="{{ route('dash.call') }}">الانتقال للرد</a>
    </div>
    <div style="background: #333; color: #fff; direction: ltr;padding: 5px; text-align: center;">
        <p>Made By
            <a style="color: orangered; text-align: center; text-decoration:none;" href="http://Cascocode.com">Casco Code</a>
            &copy; 2018 </p>
    </div>


</body>
</html>