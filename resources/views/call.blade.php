<div class="chat">
    <div class="chatbox chatbox--tray @if (Cookie::get('chat-id') == false) chatbox--empty @endif">
        <div class="chatbox__title">
            <h5><a>خدمة العملاء</a></h5>
            <button class="chatbox__title__tray">
                <span></span>
            </button>
            <button class="chatbox__title__close">
            <span>
                <svg viewBox="0 0 12 12" width="12px" height="12px">
                    <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                    <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                </svg>
            </span>
            </button>
        </div>
        <div class="chatbox__body">
            <div class="chatbox__body__message chatbox__body__message--right">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">
                <p>اهلا بك ,, تستطيع التواصل معنا دائما</p>
            </div>
            <div class="chat-msgs">
                @if (Cookie::get('chat-id') == true)
                    <?php $msgs = App\msgs::where("chat_id",Cookie::get('chat-id'))->get(); ?>
                    @foreach($msgs as $msg)
                        <div class="chatbox__body__message chatbox__body__message--left">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">
                            <p>{{ $msg->msg }}</p>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        @if (Cookie::get('chat-id') == false)
            <form class="chatbox__credentials">
                <div id="csrf">
                    @csrf
                </div>
                <div class="form-group">
                    <label for="inputName">الاسم:</label>
                    <input type="text" class="form-control bg-white p-2" id="inputName" required>
                </div>
                <div class="form-group">
                    <label for="inputEmail">البريدالاكلتروني:</label>
                    <input type="email" class="form-control bg-white p-2" id="inputEmail" required>
                </div>
                <button type="submit" class="btn btn-success btn-block">Enter Chat</button>
            </form>
        @endif
        <form id="frmsg" method="post" class="bg-white row" style="padding: 10px; margin: 0">
            @csrf
            <textarea name="msg" class="chatbox__message textmsg col-10 form-control" placeholder="الرسالة"></textarea>
            <button type="submit" id="subchat" class="btn btn-block btn-success col-2"><i class="icofont-send-mail"></i>
            </button>
        </form>
    </div>
</div>


<style>
    .chatbox {
        position: fixed;
        bottom: 0;
        right: 30px;
        width: 250px;
        height: 400px;
        font-family: 'Lato', sans-serif;
        z-index: 999999999;
        -webkit-transition: all 600ms cubic-bezier(0.19, 1, 0.22, 1);
        transition: all 600ms cubic-bezier(0.19, 1, 0.22, 1);

        display: -webkit-flex;
        display: flex;

        -webkit-flex-direction: column;
        flex-direction: column;
    }

    .chatbox--tray {
        bottom: -350px;
    }

    .chatbox--closed {
        bottom: -400px;
    }

    .chatbox .form-control:focus {
        border-color: #1f2836;
    }

    .chatbox__title,
    .chatbox__body {
        border-bottom: none;
    }

    .chatbox__title {
        min-height: 50px;
        padding-right: 10px;
        cursor: pointer;

        background-color: rgba(18, 105, 144, 0.66);
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;

        display: -webkit-flex;
        display: flex;

        -webkit-align-items: center;
        align-items: center;
    }

    #frmsg{

        padding: 10px;
        margin: 0;
        height: 100%;
        display: flex;
    }

    .chatbox__title h5 {
        height: 50px;
        margin: 0 0 0 15px;
        line-height: 50px;
        position: relative;
        padding-left: 20px;

        -webkit-flex-grow: 1;
        flex-grow: 1;
    }

    .chatbox__title h5 a {
        color: #fff;
        max-width: 195px;
        display: inline-block;
        text-decoration: none;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .chatbox__title h5:before {
        content: '';
        display: block;
        position: absolute;
        top: 50%;
        left: 0;
        width: 12px;
        height: 12px;
        background: #4CAF50;
        border-radius: 6px;

        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .chatbox__title__tray,
    .chatbox__title__close {
        width: 24px;
        height: 24px;
        outline: 0;
        border: none;
        background-color: transparent;
        opacity: 0.5;
        cursor: pointer;

        -webkit-transition: opacity 200ms;
        transition: opacity 200ms;
    }

    .chatbox__title__tray:hover,
    .chatbox__title__close:hover {
        opacity: 1;
    }

    .chatbox__title__tray span {
        width: 12px;
        height: 12px;
        display: inline-block;
        border-bottom: 2px solid #fff
    }

    .chatbox__title__close svg {
        vertical-align: middle;
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-width: 1.2px;
    }

    .chatbox__body,
    .chatbox__credentials {
        padding: 15px;
        border-top: 0;
        background-color: #f5f5f5;
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;

        -webkit-flex-grow: 1;
        flex-grow: 1;
    }

    .chatbox__credentials {
        display: none;
    }

    .chatbox__credentials .form-control {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .chatbox__body {
        overflow-y: auto;
    }

    .chatbox__body__message {
        position: relative;
    }

    .chatbox__body__message p {
        padding: 15px;
        border-radius: 4px;
        font-size: 14px;
        background-color: #fff;
        -webkit-box-shadow: 1px 1px rgba(100, 100, 100, 0.1);
        box-shadow: 1px 1px rgba(100, 100, 100, 0.1);
    }

    .chatbox__body__message img {
        width: 40px;
        height: 40px;
        border-radius: 4px;
        border: 2px solid #fcfcfc;
        position: absolute;
        top: 15px;
    }

    .chatbox__body__message--left p {
        margin-left: 15px;
        padding-left: 30px;
        text-align: left;
    }

    .chatbox__body__message--left img {
        left: -5px;
    }

    .chatbox__body__message--right p {
        margin-right: 15px;
        padding-right: 30px;
        text-align: right;
    }

    .chatbox__body__message--right img {
        right: -5px;
    }

    .chatbox__message {
        padding: 15px;
        min-height: 50px;
        outline: 0;
        resize: none;
        border: none;
        font-size: 12px;
        border: 1px solid #ddd;
        border-bottom: none;
        background-color: #fefefe;
    }

    .chatbox--empty {
        height: 306px;
    }

    .chatbox--empty.chatbox--tray {
        bottom: -255px;
    }

    .chatbox--empty.chatbox--closed {
        bottom: -262px;
    }

    .chatbox--empty .chatbox__body,
    .chatbox--empty .chatbox__message {
        display: none;
    }

    .chatbox--empty .chatbox__credentials {
        display: block;
    }

</style>


<script>
    (function ($) {
        $(document).ready(function () {
            var $chatbox = $('.chatbox'),
                $chatboxTitle = $('.chatbox__title'),
                $chatboxTitleClose = $('.chatbox__title__close'),
                $chatboxCredentials = $('.chatbox__credentials');
            $chatboxTitle.on('click', function () {
                $chatbox.toggleClass('chatbox--tray');
            });
            $chatboxTitleClose.on('click', function (e) {
                e.stopPropagation();
                $chatbox.addClass('chatbox--closed');
            });
            $chatbox.on('transitionend', function () {
                if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
            });
            $chatboxCredentials.on('submit', function (e) {
                var
                    $inputName = $("#inputName").val(),
                    csrf = $("#csrf input").val(),
                    $inputEmail = $("#inputEmail").val();

                $.ajax({
                    url:"{{ route('chat.make') }}",
                    type:"POST",
                    data:{_token:csrf,name:$inputName,mail:$inputEmail},
                    success:function(){
                        $chatbox.removeClass('chatbox--empty');
                    }
                });
                setInterval(function () {
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        url:"{{ route('msg.get') }}",
                        type:"POST",
                        dataType:"JSON",
                        data:{_token:"{{ csrf_token() }}"},
                        success: function (data) {
                            $(".chat-msgs").empty();
                            $.each(data, function (i,msg) {
                                if ( msg.admin == 0 ){
                                    $(".chat-msgs").append(
                                        '<div class="chatbox__body__message chatbox__body__message--left">\n' +
                                        '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                                        '            <p>' + msg.msg + '</p>\n' +
                                        '</div>\n');
                                } else {
                                    $(".chat-msgs").append(
                                        '<div class="chatbox__body__message chatbox__body__message--right">\n' +
                                        '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                                        '            <p>' + msg.msg + '</p>\n' +
                                        '</div>\n');
                                }

                            });
                        }
                    });
                },500);

                e.preventDefault();
                $chatbox.removeClass('chatbox--empty');
            });


            $.ajaxSetup({

                headers: {

                    'X-CSRF-TOKEN': "{{ csrf_token() }}"

                }

            });


            $("#subchat").click(function (e) {

                e.preventDefault();

                var msg = $("[name=msg]").val();
                var token = "{{ csrf_token() }}";
                $("[name=msg]").val('');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('msg.send') }}',
                    data: {_token:token, msg: msg,},
                    success: function (data) {
                        $("[name=msg]").val(' ');
                        setInterval(function () {
                            var token = "{{ csrf_token() }}";

                            $.ajax({
                                url:"{{ route('msg.get') }}",
                                type:"POST",
                                dataType:"JSON",
                                data:{_token:"{{ csrf_token() }}"},
                                success: function (data) {
                                    $(".chat-msgs").empty();
                                    $.each(data, function (i,msg) {
                                        if ( msg.admin == 0 ){
                                            $(".chat-msgs").append(
                                                '<div class="chatbox__body__message chatbox__body__message--left">\n' +
                                                '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                                                '            <p>' + msg.msg + '</p>\n' +
                                                '</div>\n');
                                        } else {
                                            $(".chat-msgs").append(
                                                '<div class="chatbox__body__message chatbox__body__message--right">\n' +
                                                '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                                                '            <p>' + msg.msg + '</p>\n' +
                                                '</div>\n');
                                        }

                                    });
                                }
                            });
                        },1000);
                    }
                });
            });
        });

    })(jQuery);
</script>


<script>
    @if (Cookie::get('chat-id') == true)
    setInterval(function () {
        var token = "{{ csrf_token() }}";

        $.ajax({
            url:"{{ route('msg.get') }}",
            type:"POST",
            dataType:"JSON",
            data:{_token:"{{ csrf_token() }}"},
            success: function (data) {
                $(".chat-msgs").empty();
                $.each(data, function (i,msg) {
                    if ( msg.admin == 0 ){
                        $(".chat-msgs").append(
                            '<div class="chatbox__body__message chatbox__body__message--left">\n' +
                            '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                            '            <p>' + msg.msg + '</p>\n' +
                            '</div>\n');
                    } else {
                        $(".chat-msgs").append(
                            '<div class="chatbox__body__message chatbox__body__message--right">\n' +
                            '            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABblBMVEWavFX///8+Pj9LS0z8uH6DUj7jrX6ERi3m5uZtrrxoucv4tn6Xuk+bv1aWuUyUuEiSt0Ls7OybwleCSj06OD7j7NOVvFODPivosYH/vIHw9edJSEw2Njc3ND5FRUa3z4vU4rufv16oxW9HRUxmdUj2+fDd6MokJCUxMTJ+PyjB1ZzJ2qmjwmVEQEuxy4CTm0lnZ2jc3dnGumfiuXPF2KOCOCnF1tqYs1PR0dGQikOhoaF7e3vwuXm1ztSWmJOuu126u2KVZEq2uLSUpU+IV0Gpd1i4hWLIlGy0trF2dnd/mE40Lz2gxMxtb2lYWlXc7vKr1uCIcESNfT/Uk2SSVDeKbTmAw9NPV0OktId5jlGKc0SGucTV2M+HVzKPh0JneEhITUFeZ058lE2wy9LB4OimsWPLrXS5rm2GZEL917rm0cPjtIzM1rrioG3kw6imaEb8w5T+5dPBglimllu+lmiiolmoua9bbTnSrYi6uKYaBbpLAAAQz0lEQVR4nM3d618TVxoH8DPBKGomk4RCFdOEBMIlBCi71ICstBpvZRURLyy1ysW22rq123a3rf/9npnJZS7nnDnP70zQ50376QvNt8/znNvcmDX8mByfnVpsVpfrrVaNMVZrterL1ebi1Ox44xT+djbMP7wxPtWs1/IFx8nnczkWjFwun3ecQr5Wb04NFzosYWN2rc5pUVg8OJVD62uzw2IOQ9iYrdZ42pJsISdPaK06FGXqwvFmq+BQcAGmU2g1x9P+QekKZ6t5UNeLvJOvzqb6m1IUzlYdQ14vlaki0xJOruVS4fWQubXJlH5ZOsKpeiGfGs+PfKE+lcpvS0HYWEynOqORc5zFFAZXY+Fk1Uk7fYPIO1XjYjUUTlYLw0jfIHKFZUOjkXByeSjlGTE6ZkYDYWN5yPnrGwvLBv2IC5un5POMTvPUhVO54Y0vosjn0LkDE062nFP1ueG0sHaEhM3CqfvcKEClCgjH2ekW6CDyDNh50IXVD5NAPwrVoQsnax8qgX7ka9RuJAoXP2QC/SgsDlHYqJ/+EBoPp06a/ynC8cRjpdOJXI4y4BCEH0GF9oJSqfrC5Y+hQnvhLKcubLQ+7BgajXxLtxk1hZMfSQsOIpfTnDb0hOMfU4X2wtEbb7SEU6mMMbYbJT+8fzf9Awta2w0doTmQy2zWWdnY23v69M2bR4++2Xu5sdJh/L8aMbWIGkLTWcIusc7G0xdffOrGwrmsF2NuvPlmo+PacaLGrJEsNAPadnvjjSfrRTYYY2PZRyttHKlBTBSaAO1S+yXP3blQZKPBc7nBUGQyMUloALTZSownEnrIRx2wJROJCUJ8kCmxl+fiPInQRb7ZwIxJw41aCANttvep0CcVcmMWMyYQlcJxEGjbGwsSn0LIjbdWSghROfWrhJPgSqbUeSL1KYXc+KgNpNFRLeAUwgbms9nTL+S+BCE3biCVqliGK4QtaLFdWjm3oAImCfmQQ09jroUIl6Htkr2nTKCGkBvp3ZiX7xelwkWkCe32C0UH6gqzY3vkLDrSaVEmhIZRu5NQoZpCXqmMapQOqBJhA+nB0kpiAjWF2ewtcjPmJKONRIiMMqWNpBYkCLNZKjFXpwjXgCYsbehkUF9IJjpr+kKkCTUzSBCOUYniVhQK6T59oL6QnkUxRvDfqvSZ0F7RBVKEt4i/Ii+6MiUQztJr1O7o9SBRmH1DrVPB/XACITJRaPtowrFviKubnI6wCdToi+SJHhKS1+H5+IXwmHCSXqOlp/o1ShRmxzo0YiG2kYoJa2QgYZShC8mjTS1JOAXM9RQfWUhtRSd6phEV0ocZWo2SheQ6jQ42ESF9mCHWKF2YvUUcbNZUwgYwFdJ8gJA6njoNhbBKLtLSS1qNAsLsGO0n5apyITBTtKlARLhHG2zCM0ZIuExP4Z5AaHYSJSK2ST8qtywTAilkceDCk5+fqIyI0CSJQSE9hXY8hQu/nLl8RkUEhOROXBYLkRTGM/jb5TNqIiR8ia/dAkL6QGrHDi4WHnOgG3IiIqTOicHhdCBs0NdrpRcR35OLXeCZy7/IiJBwbAWeEwfCRfpyphNezvAK7QE58TcJERJmH9HGmvyiQAikMDTOLJz7eeBziY/FRExInDCYExcCm4rQgs0bQ0Nx+WchERRSy3QqJqzTx5lAkfJJMOJziRdF4w0mpJbp4Hy4JwSmisFkuODNEYIQjTegkDglDiaMnrAJbAx7I+nCLxfFQGEzokLqNrEZEQIHbN1Ft7BAFZWKCqlX3HJh4SwwznhXmhbOPVb4XGJ02gCF2TfEg0VnNiSkr2d4Gy7w/D2+rAa6xvBKHBVmib+vt67pCoEr2qUXWr5uGiX3tRGC2ogsHxQCRcpsTZ/fjYNBFRYSZ8RemTK0SNmX2j7P+Li3MUaFWfJQUw0IgSKd+BvB55eqb4SFxDm/V6aeELmNmyp0d41eO8LCW1ShfyM4A6d7NvF3qrBrhIVjVKE/6XvCFh0ICd0h5zdcSP6NrZ4QOAfmwn8gQh5n79wChcQNFF+bNrpC5GoMLLx49tKluzdOR+htoRg4VxgIeVxCEkme8v35whXSLxmaCs+6ibxNRNKFrOYLgSMocyGABITugRTDlmxpCD3k2TvaPYkIZz3hGnQjKTZbRIQ+8q6eEhC61xIZckKTptBX8lzevpEdS1vontYw5BgxbWFPybN5+4a0NemzhXeoyKDLFQxZl6qFfadbtnfv3Llz+/YNN265wf95+85d4FcWJrkQfHpyOMKQNBJ3J+i/ki++GXCa7wu/GqZQFIgwv8iF0IqGx5enLfwvIOSrGmbVMSBjpy38HRCyOheiQHDKx4X/gx5t40L0MW1wusCFHeRXOhYDJwt4MIWFd4Hp0J0uGP6oPTbUwEJkoHGnC4atu92YOFXhpd+hNnRm2RT8vgusEeEcUg+E/chPsTX4hRdYI6JCZL7nkVtkyEliN2qnKXyNPeqdazJ0ScPAGREVQiOpu6hhyzAQK1NQ+ANWpIz74EWbG6cn/B59sUSdIefdvUBGU1AI/8aWkRCZ9DHha7RIuQ86LO0FMNZgQnCc4WHkg7bBkBAeZ4yFQBIhIfKihZSM9CQiQoMUcp9pnVKTiAhNUlgzG0sZfTgFhPhAyoxnC0afEwGhUZm1zNY0XgxbCC9nvKibC4mrU7LQZJjxhAZ7ix6RVKdkodFM4e4tDPaH/Rim0KxG3f0hvsfvB2lSJAqNxlHm7fHxc5oAkdCKNOEPpj8tP5WKkNKKNKFZEzLvrC2dV5PqL21IQmOge14Kn3lH4uIQhIajjBuFSfy6RTTSF35vOMq44Rhce4rEhObZor4wDaB37cl82ebHhF6hagvTAdYNrgELQme40RWmA/SuAYPX8UWhM2loCs1HUS+86/hpvslag6gl/CEloH8vRlrThRfJCzgd4euJlID+/TSpTRdeTNQSmlFDmE4LeuEY3NcmjYRFaqIwtQpl/fvasHsT5TFRU3VjkvD71CqU9e9NxK9zy0LVjWrha/IbIZXRvb8Uu0dYGRPyUlUJX7fT60AvuvcIY/d5qyNXcCRGuZD7Uk0g69/nneaqhnmf9i3Um+6nfX/9j77w7U/fLrpfEU73p1RNnrcIhB2K5cXBCxobP8bWqkLhu9VRL1b/3az3/zhzYf95C+iZma6sVGLtzvbJweGMH38WrXD8+uMlhfASz967n0YDUVz9c+bw4OBku9O2/e9g4ML+MzPAc0/e39w5OZzZz3zmxSfdmFu3ovHs/B/vLsWE3s3Af72/NxqJ4s1PrkzzWOIxsj9zuN1mMLP/3BPx2TX3cxydg+Mjz5WJRHkrJrQuuPH8j/fv3r7lRevK3v711/v3fzw/f/58FDha3KqM9OMKxy5V9mdO2ogyP3h2jbD4dlN3cCyydWMkLty9EIzzwbhfjAl3RqLBnUtLxwdtKjLw/KHuM6T8r9ieych1bszFhZZcGEvh6GomJvSZ00tHh22SMfAMqd58YZc6M598ptK5MR9vxHASg8BX8RTelAjd4MgD/Q+ahJ4D1li42fbJfiLPFV5LSKI6haE2FCGnj3W/9RF6ljuxTG12mNHx8aHmc4HwO7HwukD4uRLoluvS/rZWHkPP4yeUqW0fZD7T4XlEgdASC+PA0VFFkQaMGg8IRd6poCzT0vaRto8PNavqJKpTuK4hdI0ziZuQyHsxFO82sdnxP/V9kka0BMLnggwWr6nbcGC8sp1AjLzbRD7plzoZrf4bVOnXIuHDuPCqSPi1pnBkZGlGOeLE3k8jO48qHZAS6CVRJLSeRYXPYzOFG7o+HtNHqvOO2DuGJKc19jGhA7sxN6pMoiqFo3pt2KvUafmAE39PlGQLtU8HZuZvKpOoSmHxGkXIK1XajIJ3fYkOFe19Wgv6Ud4UCnfDwvuiFBY3tdswgSh4X5vgdB8DChffAqEwhzQfj2lxLwrfuRc7kCrNYMDMXHQXrF2lpDb04sqI8NZT4XsTo+samz6KdkPciBpC5bJbRtwXJFH87svIhGG3gUHGD9EuOCY8LxImLLuFMX0YJ0reXxp+B619BNZoRtKIOsLEZbcolmJzhuwdtKEk2gdwCnmZihoxOh+KxlJ6kfK4shMVSt8jHExiG8+geBesIUTa0I3pgzBR/i7oQBLhcdQLYSNGhatxoe6yO5ZFRQpl72THhxkvRLvg5FVb8XNQGE6i6p3s/TnRPjRJofo4SrEuBYE8iUGh8r36/WuJZkBRI8ZW3vH9L32+7yfxZEBUfxuhuxO2T8yKVLQLThbCbchzeDwQJnzfwt9ilMAFaT8Ex1Ex4auYkLrsDiaxv3ZL/EaJfw3DMIWi46iYMLa5oC+7A8L+WBP7mqXoW0HGRSo6jvouJowuavA2DJSpxreCrGbeNpoMvZi/lyiMLr2L9wyEI9O+UOd7T3ywKR2ZAgXHUbuJQoM2HCxO4xzRd9ccdNsUJCYKo8s2wUUnSg69RtT87pr1L+M2FBxHJQplF5304spMSf/beZZlDozvgpOE6LK7J9wvCWtUIlyfMxbGjqNi56VRIbL7DQbpG5bW1rwxMboLjgvDmwts9zuIJdJ3SC1rp2wqjB5HxYWRpbdRkXJhW/LlaomwaJzEaCNeUAs1LzrJY3qb9j1g81aM7oLjwtDS22DZ7Uf5W4lE+l1u41aMNGJcGFp6Ey46CSMjvKanFFoPDFsx0ohxYXjpbZbBygOpQy40HW0iu+AEoVkbVnbkDIWwaCgM1Y3gCmlwYUq96BQVCq8jJAqtVbPRJrQLThIaLbvLolsHdISGA2roWnCS0CSDZdHxrJ7QumdCDN2mKBAGz/VN2rAc24sShEbEUCN+JxCms+xOACYJrWs4MXQclSDEl91l6USoKbS2cGKwEROE8LJbfCWPJDQgBo+jdgXCweYC3v0mAzWEeKEGj6NEwqvGbZhYonpCeLgJHkephWAbJg0y2kJ8RB38Ec8EwuuGbVgW37iDCNGpP3AcJRIONhdQkWaUEz1RaK2WkUVqYBcsEvaW3tDut1JRLdXoQqs4AhADx1EXRMIivvut7CgW25CQ7xeBLXFFKewtTIHdb0a+H8SFyMQ4uClDJaTvfjWmQURorc9TK3XQiCJhb+lNbcOK5hhDF1rFI2KlDv5fC4XdIiVedMpotyBdSK/U3unCQ5WQtvulVCggtNYzpDT2jqOUQlKFjlAqFBFa1iYljb3jKJWQ0oaSu3PTFVrrZf009nbBYuFV4rI7U6EmEBOSurHbiMInSrpC7WU3tQMNhNbqiG4ay8lCzWV3ZkdzmZaKkG83NEu1exwlFl7XX3ZXKjo7pTSFbqnqzP/dRtwVCl/ptmEFK1BDoTX6QMPYPY4SC+/rLbsr5U3hQypDF/J21DDOq4RFjVsuK+UHWAOmIdQx+sdRz4RCb+k9XJ+xkBs355Vjjn8cJRcq5/tKZtPQl4KQr8e35hRGfxUiFvLNhaoN+cKdtMYWRwpCHvd25MXq7YIviIWKZXelvKN10JQY6Qh5sW7NS3aP3nGUXCipzsyWcXl2Iy0hj5sPhEhvFywVCtqQ8zbTSZ8XKQotMdJrRIlwNbb7TZlnpS3ksb51NBdRWlLh1VAbVjLlnS1g96CO1IU8ivc2KwGluwuWCru3XFa4bmTzZgpDZyyGIXSjeHNrZ27Oq1m+C34oE65m3Loslx9sDUXnxrCEXoyuX9vcycxtSoXXb2Z2Nq+tG6w6k2OoQj/4tF7c3d199iwgfH7//qvrVx8OK2/B+D9U0zHTTOnksQAAAABJRU5ErkJggg==" alt="Picture">\n' +
                            '            <p>' + msg.msg + '</p>\n' +
                            '</div>\n');
                    }

                });
            }
        });
    },500);
    @endif
</script>
