<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>دفع الفواتير</title>
    <!-- The Styling File -->
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Cairo|Open+Sans" rel="stylesheet">
    <style>
        /**
 * The CSS shown here will not be introduced in the Quickstart guide, but shows
 * how you can use CSS to style your Element's container.
 */
        * {
            font-family: 'Cairo', 'Open Sans', sans-serif;
        }

        body {
            background: url("{{ asset('public/home/dist/imgs/about.jpg') }}");
            background-size: cover;
        }

        body::before {
            content: "";
            width: 100%;
            height: 100%;
            background: #333;
            opacity: 0.2;
            position: absolute;
        }

        .container {
            position: relative;
            z-index: 50;
        }

        .StripeElement {
            background-color: white;
            height: 40px;
            padding: 10px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
</head>
<body>
<div class="container text-right">
    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-right bg-dark">
                    <img src="{{ asset("public/css/ic_logo.png") }}" height="70px">
                    <h1 class="float-left text-white">فاتورة #{{ $inv->id }}</h1>
                </div>
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-md-6 text-center">
                            <img style="width: 100px" class="" src="https://image.flaticon.com/icons/svg/214/214343.svg" alt="">
                            <h5>عملية شراء امنة </h5>
                            <h5 class="p-2" style="background: #eee; color: #0a6ebd">{{ $inv->amount }} التكلفة:</h5>
                        </div>
                        <div class="col-md-6">
                            <h5 class="text-right">معلومات عن الفاتورة</h5><br>
                            <p class="text-right">
                                {{ $inv->name .' :الاسم' }}<br>
                                {{ $inv->apartment .' :الغقار' }}<br>
                                {{ $inv->email .' :الالبريد الالكترونى' }}<br>
                                {{ $inv->notes .'  : ملاحظات' }}<br>
                            </p>
                        </div>
                    </div>
                    <form class="" action="{{ route("home.invoice.pay",$inv->id) }}" method="post"
                          id="payment-form">
                        @csrf
                        <label for="card-element"> ادخل بطاقة الائتمان أو الخصم </label>

                        <div class="form-row text-left">
                            <div id="card-element" class="form-control col-md-10">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <!-- Used to display form errors -->
                            <button id="subformpay" class="btn btn-primary col-md-2">Submit Payment</button>
                            <div id="card-errors"></div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Stripe JS -->
<script src="{{ asset('public/home/dist/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/bootstrap.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>

<!-- Your JS File -->
<script>
    // Create a Stripe client.
  //  var stripe = Stripe('{{ config("app.strip.public") }}');
    var stripe = Stripe('{pk_test_FGbg5ktICr4pRayYYUNiiOe2');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            lineHeight: '18px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    // Handle form submission
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                $("#subformpay").hide();
                stripeTokenHandler(result.token);
            }
        });
    });

    // Send Stripe Token to Server
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
// Add Stripe Token to hidden input
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
// Submit form
        //   console.log(token);
        form.submit(function (e) {
            e.preventDefault();
        });
    }
</script>

</body>
</html>
