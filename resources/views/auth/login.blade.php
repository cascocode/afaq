<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf" content="">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <title> آفاق المستقبل العقارية</title>
    <link rel="icon" href="{{ asset('public/home/dist/imgs/logo.png') }}">

    <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstraps.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/jquery-ui.css') }}">


    <link rel="stylesheet" href="{{ asset('public/home/dist/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/full-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/icofont.min.css') }}">


</head>
<body>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div class="footer">
    <footer class="bg-dark py-3" style="background-color: #0f0000 !important;">
        <div class="container ">
            <div class="row social">
                <a href="https://www.instagram.com/afaq_future_realestate.co/" class="noback"><img src="{{ asset('public/insta.svg') }}" alt=""></a>
                <a href="" class="twit noback"><img src="{{ asset('public/twit.svg') }}" alt=""></a>
                <a href="https://m.facebook.com/afrkw/" class=" noback"><img src="{{ asset('public/face.svg') }}" alt=""></a>
                <a href="https://www.snapchat.com/add/afaqfuture00965" class="noback"><img src="{{ asset('public/snap.svg') }}" alt=""></a>
                <a href="https://www.youtube.com/" class="noback"><img src="{{ asset('public/you.svg') }}" alt=""></a>
            </div>
            <p class="m-0 text-center text-white">Copyright &copy; Your <a href="https://cascodcode.com" id="cascocode"
                                                                           target="_blank" class="text-info">Cascocode
                    Team</a> 2018</p>
        </div> <!-- footer-copyright -->
    </footer>
</div>

<script src="{{ asset('public/home/dist/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/plugin/wow.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/main.js') }}"></script>

<script>
    new WOW().init();
</script>
<!-- Scripts -->
@stack('js')

</body>
</html>