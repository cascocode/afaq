<?php error_reporting(0); ?>
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <title>لوحة تحكم افاق </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/dash/css/main.css') }}">

    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>


    <link href="{{ asset("public/dash/bootstrap-fileinput/css/fileinput.css") }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("public/dash/bootstrap-fileinput/themes/explorer-fa/theme.css") }}" media="all" rel="stylesheet" type="text/css"/>

    @stack("css")
</head>
<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="index.html">Vali</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href=""><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                <li><a class="dropdown-item" href="javascript:void(0);" onclick="document.getElementById('logout-form').submit()"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            </ul>
            <form action="{{ route('logout') }}" method="post" id="logout-form">
                @csrf
            </form>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    @include('dash.components.menu')
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <h1><i class="fa fa-dashboard"></i> {{ $title or 'لوحة التحكم' }}</h1>
        </div>
    </div>
    <div class="tile">
        @yield("content")
    </div>
</main>

<!-- Footer -->
<div class="footer">
    <footer class="bg-dark py-3" style="background-color: #0f0000 !important;">
        <div class="container ">
            <p class="m-0 text-center text-white">Made By :  <a href="https://cascodcode.com" id="cascocode"
                target="_blank" class="text-info">Cascocode
                    Team</a>  &copy;
 <span> {{ date("Y") }}</span> </p>
        </div> <!-- footer-copyright -->
    </footer>
</div>

<!-- Essential javascripts for application to work-->
<script src="{{ asset('public/dash/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('public/dash/js/popper.min.js') }}"></script>
<script src="{{ asset('public/dash/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/dash/js/main.js') }}"></script>

{{----------}}
<script src="{{ asset("public/dash/bootstrap-fileinput/js/plugins/sortable.js") }}" type="text/javascript"></script>
<script src="{{ asset("public/dash/bootstrap-fileinput/js/fileinput.js") }}" type="text/javascript"></script>
<script src="{{ asset("public/dash/bootstrap-fileinput/js/locales/ar.js") }}" type="text/javascript"></script>
<script src="{{ asset("public/dash/bootstrap-fileinput/themes/explorer-fa/theme.js") }}" type="text/javascript"></script>
<script src="{{ asset("public/dash/bootstrap-fileinput/themes/fa/theme.js") }}" type="text/javascript"></script>


<!-- The javascript plugin to display page loading on top-->
<script src="{{ asset('public/dash/js/plugins/pace.min.js') }}"></script>
<!-- Page specific javascripts-->
<!-- Google analytics script-->
@stack("js")
</body>
</html>
