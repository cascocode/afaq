@inject('function', 'App\Http\Controllers\Functions')
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <title> آفاق المستقبل العقارية</title>
    <link rel="icon" href="{{ asset('public/home/dist/imgs/logo.png') }}">

    <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstraps.min.css') }}">
    @if(app()->getLocale() === "ar")
        <link rel="stylesheet" href="{{ asset('public/home/dist/css/bootstrap/bootstrap-rtl.min.css') }}">
    @endif

    <link rel="stylesheet" href="{{ asset('public/home/dist/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/full-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('public/home/dist/css/main.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

    <link rel="stylesheet" href="{{ asset('public/icofont.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ekko-lightbox.css') }}">

    @stack("css")
    <style>
        #nvlogo {
            position: absolute;
            width: 200px;
            hight: 500px;
            top: 90px;
            box-shadow: none;
        }

        #thenav {
            position: relative;
        }

        #slide {
            margin-top: 182px;
        }

        .noback {
            background: rgba(0, 0, 0, 0);
        }

        .noback {
            background: rgba(0, 0, 0, 0) !important;
        }

        .noback img {
            width: 25px;
            height: 25px;
        }

        * {
            font-weight: bolder;
        }

        @media screen and ( max-width: 450px) {
            #nvlogo {
                position: relative;
                top: 0;
            }
        }

    </style>

</head>
<body>

<nav id="navigation" class="" style="position: relative">
    <nav class="navbar navbar-dark text-white py-3" id="navinfo">
        <div class="container">
            <div class="col-md-2 wow fadeInLeft">
                <div style="text-align: right"><i class="icofont-ui-clock"></i> <i class="timenow"></i></div>
            </div>
            <div class="col-md-6 wow fadeInRight">
                <div class="loc">
                    <i class="icofont-location-pin"></i> <span>{{ trans('main.loc') }}</span>
                </div>
            </div>
            <div class="col-md-3 wow fadeInRight">
                <div dir="ltr">
                    <div class="social">
                        @foreach($function->getsocial() as $s)
                            @if($s->name == "insta")
                                <a href="{{ $s->url }}" class="noback"><img src="{{ asset('public/insta.svg') }}" alt=""></a>
                            @endif
                            @if($s->name == "twit")
                                <a href="{{ $s->url }}" class="twit noback"><img
                                        src="{{ asset('public/twit.svg') }}" alt=""></a>
                            @endif
                            @if($s->name == "face")
                                <a href="{{ $s->url }}" class=" noback"><img
                                        src="{{ asset('public/face.svg') }}" alt=""></a>
                            @endif
                            @if($s->name == "snap")
                                <a href="{{ $s->url }}" class="noback"><img
                                        src="{{ asset('public/snap.svg') }}" alt=""></a>
                            @endif
                            @if($s->name == "yout")
                                <a href="{{ $s->url }}" class="noback"><img src="{{ asset('public/you.svg') }}"
                                                                            alt=""></a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-1 wow fadeInLeft">
                <div dir="ltr">
                    <div class="social">
                        <a @if(app()->getLocale() === "ar") href="?lg=en" @else href="?lg=ar" @endif
                        class="btn btn-link text-white">
                            @if(app()->getLocale() === "ar")
                                EGLISH <i class="icofont-globe"></i>

                            @else
                                العربية <i class="icofont-globe"></i>

                            @endif
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="navbar p-0 navbar-dark" id="nvlogo">
        <img src="{{ asset("public/home/dist/imgs/ic_logo.png") }}">
    </div>
    <nav class="navbar navbar-expand-md text-white" id="thenav">
        <div class="container">
            <button style="color: white" class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="icofont-navigation-menu"></span>
            </button>
            <div class="row">

                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link scroll" style=" padding-left: 135px; "
                               href="{{ route('home') }}">{{ trans('main.home') }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ trans('main.about') }}
                            </a>
                            <div class="dropdown-menu" style="color: #333 !important;"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('about1') }}">{{ trans('main.a1') }}</a>
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('about2') }}">{{ trans('main.a2') }}</a>
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('about3') }}">{{ trans('main.a3') }}</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                {{ trans('main.projects') }}
                            </a>
                            <div class="dropdown-menu" style="color: #333 !important;"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('co.project') }}">{{ trans('main.com') }}</a>
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('noco.project') }}">{{ trans('main.nocom') }}</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ trans('main.media') }}
                            </a>
                            <div class="dropdown-menu" style="color: #333 !important;"
                                 aria-labelledby="navbarDropdownMenuLink">
                                <a style="color: #333 !important;" class="dropdown-item"
                                   href="{{ route('archives') }}">{{ trans('main.archive') }}</a>
                            </div>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link scroll" href="{{ route('contact') }}">{{ trans('main.callus') }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</nav>


@yield("page")
@yield("content")

<!--hide Footer if not home-->
@if(!Request::is('/'))
    <style>
        .footer {
            visibility: hidden;
        }
    </style>
@endif

<div class="footer">
    <footer class="bg-dark py-3" style="background-color: #0f0000 !important;">
        <div class="container ">
            <p class="m-0 text-left text-white">Copyright &copy; Your Red Developments / <a
                        href="https://cascodcode.com" id="cascocode"
                        target="_blank" class="text-info">Cascocode
                    Team</a> 2018</p>
        </div> <!-- footer-copyright -->
    </footer>
</div>

<script src="{{ asset('public/home/dist/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/plugin/wow.min.js') }}"></script>
<script src="{{ asset('public/home/dist/js/main.js') }}"></script>
<script src="{{ asset('public/ekko-lightbox.js') }}"></script>
<script>
    $('#carouselExampleIndicators').carousel({
        interval: 2500
    });

    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    $(window).on('load', function () {
        setTimeout(function () {
            $("#loading").fadeOut(5000);
            $("#loading").remove();
            $("body").css({
                overflow: "auto"
            });
        }, 3000);
    });
    new WOW().init();
</script>

<!-- Scripts -->
@stack('js')
@include('call')

</body>
</html>
