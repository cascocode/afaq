@extends('layouts.app')

@section('page')
    <style>
        #navigation{
            position: relative;
        }
    </style>
        <div class="text">
        </div>
    </section>

    @if(app()->getLocale() === "ar")
    <!-- Page Content -->
    <section id="page" class="py-4">
        <div class="container">

            <!-- Our Vision Section Nad More -->
            <section id="about" class="py-3">
                <div class="container p-2">
                    <div class="row cont">
                        <br>
                        <div class="row">
                            <div class="col-md-12" style="text-algin:right;">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <h1> الرؤية و الرسالة</h1>
                                <p>
                                <h2>رؤيتنا :</h2>
                                <br>
                                تطوير مشاريع ذات جودة عالية تشكل إضافات مميزة و علامات فارقة للسوق الكويتي و تعزز الطابع المعماري و الثقافي لدولة الكويت و المنطقة و تنفيذها بمفهوم يشجع الابتكار و الابداع بالتعاون مع أفضل الخبرات في هذا المجال .
                                <br>
                                <h2>مهمتنا :</h2>
                                <br>
                                -القيام بإدارة و صيانة العقارات و المشاريع باحتراف و تقديم مستوى متميز من الخدمات و المنتجات لعملائنا و المجتمع كوننا الشريك المختار و المفضل الموثوق به.
                                -تنفيذ مشاريع عقارية تشغيلية مدرة للدخل على درجة عالية من الجودة و بمستوى منخفض من المخاطر.
                                -التخطيط السليم للالتزام بالفترة الزمنية المحددة و الميزانية المخصصة للتنفيذ ، وادارة مشاريعنا و محافظتنا علي نظرة شاملة لتقديم خدمات متكاملة .
                                -المساهمة بكل ما هو ايجابي للمجتمع .
                                -الثقة و النزاهة و الاحترام المتبادل مع عملائنا .
                                -الوفاء بالالتزمات المترتبة على الشركة تجاة العملاء .
                                <br>
                                <h2> أهدافنا : </h2>
                                <br>
                                -المحافظة على المكانة الريادية لآفاق المستقبل العقارية على مستوى الكويت و الخليج .
                                -زيادة حصتنا السوقية وتنمية محفظتنا العقارية من خلال اقتناص الفرص الواعدة .
                                -مواصلة تعزيز سياسات الحوكمة و ادارة المخاطر لمشاريعنا ، و العمل على المحافظة علي تنويع محفظتنا العقارية .
                                -التوسع الاقليمي و الدخول إلى أسواق جديدة جذابة من خلال الاستفادة من خبرتنا و مواردنا .
                                -المساهمة في مشروعات التوسع الاسكاني و بناء المناطق السكنية النموذجية الجديدة .
                                -تطوير خدمات ما بعد التأجير حفاظا علي نسب إشغال مرتفعة .

                                </p>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </section>
            <hr style="margin: 0;height:10px;background: #eee">


        </div>
    </section>
    @else
        <section id="page" class="py-4">
            <div class="container">

                <!-- Our Vision Section Nad More -->
                <section id="about" class="py-3">
                    <div class="container p-2">
                        <div class="row cont">
                            <br>
                            <div class="row text-left p-4">
                                <div class="col-md-12">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <h1> الرؤية و الرسالة</h1>
                                    <p>
                                    <h2>رؤيتنا :</h2>
                                    <br>
                                    تطوير مشاريع ذات جودة عالية تشكل إضافات مميزة و علامات فارقة للسوق الكويتي و تعزز الطابع المعماري و الثقافي لدولة الكويت و المنطقة و تنفيذها بمفهوم يشجع الابتكار و الابداع بالتعاون مع أفضل الخبرات في هذا المجال .
                                    <br>
                                    <h2>مهمتنا :</h2>
                                    <br>
                                    -القيام بإدارة و صيانة العقارات و المشاريع باحتراف و تقديم مستوى متميز من الخدمات و المنتجات لعملائنا و المجتمع كوننا الشريك المختار و المفضل الموثوق به.
                                    -تنفيذ مشاريع عقارية تشغيلية مدرة للدخل على درجة عالية من الجودة و بمستوى منخفض من المخاطر.
                                    -التخطيط السليم للالتزام بالفترة الزمنية المحددة و الميزانية المخصصة للتنفيذ ، وادارة مشاريعنا و محافظتنا علي نظرة شاملة لتقديم خدمات متكاملة .
                                    -المساهمة بكل ما هو ايجابي للمجتمع .
                                    -الثقة و النزاهة و الاحترام المتبادل مع عملائنا .
                                    -الوفاء بالالتزمات المترتبة على الشركة تجاة العملاء .
                                    <br>
                                    <h2> أهدافنا : </h2>
                                    <br>
                                    -المحافظة على المكانة الريادية لآفاق المستقبل العقارية على مستوى الكويت و الخليج .
                                    -زيادة حصتنا السوقية وتنمية محفظتنا العقارية من خلال اقتناص الفرص الواعدة .
                                    -مواصلة تعزيز سياسات الحوكمة و ادارة المخاطر لمشاريعنا ، و العمل على المحافظة علي تنويع محفظتنا العقارية .
                                    -التوسع الاقليمي و الدخول إلى أسواق جديدة جذابة من خلال الاستفادة من خبرتنا و مواردنا .
                                    -المساهمة في مشروعات التوسع الاسكاني و بناء المناطق السكنية النموذجية الجديدة .
                                    -تطوير خدمات ما بعد التأجير حفاظا علي نسب إشغال مرتفعة .

                                    </p>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </section>
                <hr style="margin: 0;height:10px;background: #eee">


            </div>
        </section>
    @endif
@endsection

