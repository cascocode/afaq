<?php

return array(
    'home'=>"Home",
    "about"=>"About Afaq",
    "a1"=>"Abstract",
    "a2"=>"Vision",
    "a3"=>"Administration",
    "projects"=>"Our Projects",
    "com"=>"Completed",
    "nocom"=>"Not Completed",
    "media"=>"Media Center",
    "archive"=>"Archive",
    "callus"=>"Call Us",
    'welcome'=>"Welcome In Afaq Future Real Estate",
    "loc"=>"الكويت .. برج مزايا الكويت 2 - الطابق 22",
    "imgs"=>"Real Life Images",
    "loc"=>"Location"
);