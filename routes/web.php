<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware("lang")->group(function () {
    Route::get('/', "HomeController@hom")->name("home");
    Route::get('/project/{id}', "HomeController@project")->name("project");
    Route::get('/projects/no', "HomeController@projectno")->name("noco.project");
    Route::get('/projects/co', "HomeController@projectco")->name("co.project");
    Route::view("/about/1", 'about-1')->name("about1");
    Route::view("/about/2", 'about-2')->name("about2");
    Route::view("/about/3", 'about-3')->name("about3");
    Route::view("contact", 'contact')->name("contact");
    Route::view("c", 'mail');
    Route::get("archs", "HomeController@archives")->name("archives");
    Route::get("arch/{id}", "HomeController@archive")->name("archive");
    Route::post("/mall", "HomeController@getmsgs")->name("msg.get");
    Route::get("chat", "HomeController@chat");
    Route::post("chat-make", "HomeController@chatmake")->name("chat.make");
    Route::post("msg-send", "HomeController@msgsend")->name("msg.send");
    Route::get('/invoice/{id}', "HomeController@invoice")->name('home.invoice');
    Route::post('/invoice/{id}', "HomeController@invoicepay")->name('home.invoice.pay');
});


Route::namespace("Dash")->group(function () {
    Route::prefix("dashboard")->group(function () {
        Route::get("/", "DashController@home")->name("dash.home");

        // Projects Routes
        Route::get("/projects/com/all", "ProjectController@compAll")->name("dash.projects.complete.all");
        Route::get("/projects/no/all", "ProjectController@nocompAll")->name("dash.projects.nocomplete.all");

        Route::get("/project/add", "ProjectController@addview")->name("dash.project.add");
        Route::post("/project/add", "ProjectController@add")->name("dash.project.addp");
        Route::get('/project/delete/{id}', "ProjectController@delete")->name("dash.project.delete");

        Route::get("project/img/add/{id}", "ProjectImgController@addview")->name("dash.project.add.img");
        Route::post("project/img/add/{id}", "ProjectImgController@add")->name("dash.project.add.imgp");
        Route::get('project/delete/img/{id}', "ProjectImgController@delete")->name("dash.project.delete.img");

        Route::get("project/edit/{id}", "ProjectController@editview")->name("dash.project.edit");
        Route::post("project/edit/{id}", "ProjectController@edit")->name("dash.project.editp");
        Route::get("project/{id}", "ProjectController@single")->name("dash.project.single");


        // Category Routes
        Route::get("cat/add", "CategoryController@add")->name("dash.cat.add");
        Route::post("cat/add", "CategoryController@addH")->name("dash.catadd");
        //---
        Route::get("cat/edit/{id}", "CategoryController@edit")->name("dash.cat.edit");
        Route::post("cat/edit/", "CategoryController@editH")->name("dash.catedit");
        //---
        Route::get("category/", "CategoryController@index")->name("dash.cat.all");
        Route::get("cat/{id}", "CategoryController@single")->name("dash.cat.single");
        //---
        Route::get("cat/del/{id}", "CategoryController@delete")->name("dash.catdelete");

        // Archive Routes
        Route::get("archive/add", "ArchiveController@add")->name("dash.archive.add");
        Route::get("archive/del/{id}", "ArchiveController@delete")->name("dash.archive.delete");
        Route::post("archive/add", "ArchiveController@addH")->name("dash.archiveadd");

        // invoice Routes
        Route::get("invoice/add", "InvoiceController@add")->name("dash.invoice.add");
        Route::post("invoice/add", "InvoiceController@addH")->name("dash.invoiceadd");
        //---
        Route::get("invoice/", "InvoiceController@index")->name("dash.invoice.all");
        Route::get("invoice/del/{id}", "InvoiceController@delete")->name("dash.invoice.delete");
        //---
        Route::get("invoice/edit/{id}", "InvoiceController@edit")->name("dash.invoice.edit");
        Route::post("invoice/edit/", "InvoiceController@editH")->name("dash.invoiceedit");

        Route::post("changePass", "DashController@changePass")->name("dash.change.pass");
        Route::post("changeImg", "DashController@changePhoto")->name("dash.change.img");

        Route::get('/calls', "CallController@showall")->name('dash.call');
        Route::post('reply/call/{id}', "CallController@re")->name('dash.re.call');
        Route::get('del/call/{id}', "CallController@dele")->name('dash.del.call');

        Route::post("get/chataj/{id}", "CallController@getchaj")->name("dash.msg.getj");
        Route::get("get/chat/{id}", "CallController@getch")->name("dash.msg.get");

        Route::post("edit/soc/{id}","DashController@editsoc")->name('dash.change.soc');

    });
});
Auth::routes();

